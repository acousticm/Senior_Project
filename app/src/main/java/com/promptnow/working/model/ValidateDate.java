package com.promptnow.working.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zephy on 8/24/2017.
 */

public class ValidateDate {

    @SerializedName("result")
    @Expose
    public List<ValiDateModel> result = null;

    public List<ValiDateModel> getResult() {
        return result;
    }

    public void setResult(List<ValiDateModel> result) {
        this.result = result;
    }

    public static class ValiDateModel {

        @SerializedName("empLeaveID")
        @Expose
        public String empLeaveID;
        @SerializedName("employeeID")
        @Expose
        public String employeeID;
        @SerializedName("leaveDateFrom")
        @Expose
        public String leaveDateFrom;
        @SerializedName("leaveDateTo")
        @Expose
        public String leaveDateTo;


        @Override
        public String toString() {
            return "ValiDateModel{" +
                    "empLeaveID='" + empLeaveID + '\'' +
                    ", employeeID='" + employeeID + '\'' +
                    ", leaveDateFrom='" + leaveDateFrom + '\'' +
                    '}';
        }

        public String getEmpLeaveID() {
            return empLeaveID;
        }

        public void setEmpLeaveID(String empLeaveID) {
            this.empLeaveID = empLeaveID;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public String getLeaveDateFrom() {
            return leaveDateFrom;
        }

        public void setLeaveDateFrom(String leaveDateFrom) {
            this.leaveDateFrom = leaveDateFrom;
        }

        public String getLeaveDateTo() {
            return leaveDateTo;
        }

        public void setLeaveDateTo(String leaveDateTo) {
            this.leaveDateTo = leaveDateTo;
        }
    }

}


