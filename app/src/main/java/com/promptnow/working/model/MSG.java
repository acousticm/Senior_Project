package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 3/23/2017.
 */
public class MSG {

    @SerializedName("result")
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("success")
        public Integer success;
        @SerializedName("employeeID")
        public String employeeID;
        @SerializedName("Token")
        public String Token;

        public String getToken() {
            return Token;
        }

        public void setToken(String token) {
            Token = token;
        }

        public Integer getSuccess() {
            return success;
        }

        public void setSuccess(Integer success) {
            this.success = success;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }
    }

}