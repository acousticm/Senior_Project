package com.promptnow.working.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephy on 9/17/2017.
 */

public class getApprovalName {
    @SerializedName("result")
    @Expose
    public Result result;

    public class Result {

        @SerializedName("success")
        @Expose
        public int success;
        @SerializedName("Fullname")
        @Expose
        public String fullname;
        @SerializedName("FullnameTH")
        @Expose
        public String fullnameTH;

        public int getSuccess() {
            return success;
        }

        public void setSuccess(int success) {
            this.success = success;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getFullnameTH() {
            return fullnameTH;
        }

        public void setFullnameTH(String fullnameTH) {
            this.fullnameTH = fullnameTH;
        }
    }
}
