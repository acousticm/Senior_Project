package com.promptnow.working.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephy on 7/26/2017.
 */

public class getApprovalItemDao {
    @SerializedName("employeeID")
    @Expose
    public String employeeId;
    @SerializedName("empLeaveID")
    @Expose
    public String empLeaveID;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("nameTH")
    @Expose
    public String nameTH;
    @SerializedName("surnameTH")
    @Expose
    public String surnameTH;
    @SerializedName("leaveDateFrom")
    @Expose
    public String leaveDateFrom;
    @SerializedName("leaveDateTo")
    @Expose
    public String leaveDateTo;
    @SerializedName("leaveNum")
    @Expose
    public String leaveNum;
    @SerializedName("leaveComment")
    @Expose
    public String leaveComment;
    @SerializedName("leaveImg")
    @Expose
    public String leaveImg;
    @SerializedName("nameType")
    @Expose
    public String nameType;
    @SerializedName("nameTypeTH")
    @Expose
    public String nameTypeTH;
    @SerializedName("leaveMax")
    @Expose
    public int leaveMax;
    @SerializedName("createDate")
    @Expose
    public String createDate;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("leaveStatus")
    @Expose
    public int leaveStatus;
    @SerializedName("leaveID")
    @Expose
    public int leaveID;
    @SerializedName("Time")
    public String time;
    @SerializedName("approveBy")
    public String approveBy;
    @SerializedName("updateDate")
    @Expose
    public String updateDate;
    @SerializedName("updateTime")
    public String updateTime;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmpLeaveID() {
        return empLeaveID;
    }

    public void setEmpLeaveID(String empLeaveID) {
        this.empLeaveID = empLeaveID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLeaveDateFrom() {
        return leaveDateFrom;
    }

    public void setLeaveDateFrom(String leaveDateFrom) {
        this.leaveDateFrom = leaveDateFrom;
    }

    public String getLeaveDateTo() {
        return leaveDateTo;
    }

    public void setLeaveDateTo(String leaveDateTo) {
        this.leaveDateTo = leaveDateTo;
    }

    public String getLeaveNum() {
        return leaveNum;
    }

    public void setLeaveNum(String leaveNum) {
        this.leaveNum = leaveNum;
    }

    public String getLeaveComment() {
        return leaveComment;
    }

    public void setLeaveComment(String leaveComment) {
        this.leaveComment = leaveComment;
    }

    public String getLeaveImg() {
        return leaveImg;
    }

    public void setLeaveImg(String leaveImg) {
        this.leaveImg = leaveImg;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public int getLeaveMax() {
        return leaveMax;
    }

    public void setLeaveMax(int leaveMax) {
        this.leaveMax = leaveMax;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(int leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public int getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(int leaveID) {
        this.leaveID = leaveID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getSurnameTH() {
        return surnameTH;
    }

    public void setSurnameTH(String surnameTH) {
        this.surnameTH = surnameTH;
    }

    public String getNameTypeTH() {
        return nameTypeTH;
    }

    public void setNameTypeTH(String nameTypeTH) {
        this.nameTypeTH = nameTypeTH;
    }
}
