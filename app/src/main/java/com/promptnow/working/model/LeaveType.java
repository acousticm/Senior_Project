package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zephy on 4/4/2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveType {

    @SerializedName("leavetype")
    @Expose
    private List<Leavetype> leavetype = null;

    public List<Leavetype> getLeavetype() {
        return leavetype;
    }

    public void setLeavetype(List<Leavetype> leavetype) {
        this.leavetype = leavetype;
    }

    public static class Leavetype {
        @SerializedName("leaveID")
        @Expose
        private int leaveID;
        @SerializedName("nameType")
        @Expose
        private String nameType;
        @SerializedName("nameTypeTH")
        private String nameTypeTH;
        @SerializedName("leaveMax")
        @Expose
        private String leaveMax;
        private ArrayList<LeaveType.Leavetype> leaveTypes = new ArrayList<Leavetype>();

        public Leavetype(ArrayList<Leavetype> leaveTypes) {
            this.leaveTypes = leaveTypes;
        }

        @Override
        public String toString() {
            return "Leavetype{" +
                    "leaveID='" + leaveID + '\'' +
                    ", nameType='" + nameType + '\'' +
                    ", leaveMax='" + leaveMax + '\'' +
                    '}';
        }

        public ArrayList<Leavetype> getLeaveTypes() {
            return leaveTypes;
        }

        public void setLeaveTypes(ArrayList<Leavetype> leaveTypes) {
            this.leaveTypes = leaveTypes;
        }

        public int getLeaveID() {
            return leaveID;
        }

        public void setLeaveID(int leaveID) {
            this.leaveID = leaveID;
        }

        public String getNameType() {
            return nameType;
        }

        public void setNameType(String nameType) {
            this.nameType = nameType;
        }

        public String getLeaveMax() {
            return leaveMax;
        }

        public void setLeaveMax(String leaveMax) {
            this.leaveMax = leaveMax;
        }

        public String getNameTypeTH() {
            return nameTypeTH;
        }

        public void setNameTypeTH(String nameTypeTH) {
            this.nameTypeTH = nameTypeTH;
        }
    }
}