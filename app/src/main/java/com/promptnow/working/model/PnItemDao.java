package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Zephy on 3/6/2017.
 */

public class PnItemDao {
    @SerializedName("employeeID") String employeeID;
    @SerializedName("name") private String name;
    @SerializedName("surname") private String surname;
    @SerializedName("nickname") private String nickname;
    @SerializedName("nameTH") private String nameTH;
    @SerializedName("surnameTH") private String surnameTH;
    @SerializedName("nicknameTH") private String nicknameTH;
    @SerializedName("email") private String email;
    @SerializedName("phoneNo1") private String phoneno1;
    @SerializedName("phoneNo2") private String phoneno2;
    @SerializedName("lineID") private String lineId;
    @SerializedName("position") private String position;
    @SerializedName("department") private String department;
    @SerializedName("imgProfileURL") private String imgUrl;
    private ArrayList<PnItemDao> personlist = new ArrayList<PnItemDao>();

    public PnItemDao(ArrayList<PnItemDao> personlist) {
        this.personlist = personlist;
    }

    @Override
    public String toString() {
        return "PnItemDao{" +
                "employeeID='" + employeeID + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", phoneno1='" + phoneno1 + '\'' +
                ", lineId='" + lineId + '\'' +
                ", position='" + position + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getSurnameTH() {
        return surnameTH;
    }

    public void setSurnameTH(String surnameTH) {
        this.surnameTH = surnameTH;
    }

    public String getNicknameTH() {
        return nicknameTH;
    }

    public void setNicknameTH(String nicknameTH) {
        this.nicknameTH = nicknameTH;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno1() {
        return phoneno1;
    }

    public void setPhoneno1(String phoneno1) {
        this.phoneno1 = phoneno1;
    }

    public String getPhoneno2() {
        return phoneno2;
    }

    public void setPhoneno2(String phoneno2) {
        this.phoneno2 = phoneno2;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getImgUrl() {
        if (imgUrl == null){
            return "";
        }
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public ArrayList<PnItemDao> getPersonlist() {
        return personlist;
    }

    public void setPersonlist(ArrayList<PnItemDao> personlist) {
        this.personlist = personlist;
    }
}
