package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 3/6/2017.
 */

public class PnItemCollectionDao {
    @SerializedName("result")
    private List<PnItemDao> data;

    public List<PnItemDao> getData() {
        return data;
    }

    public void setData(List<PnItemDao> data) {
        this.data = data;
    }
}
