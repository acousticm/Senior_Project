package com.promptnow.working.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephy on 7/8/2017.
 */

public class HistoryItemDao {
    @SerializedName("empLeaveID")
    @Expose
    public String empLeaveID;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("nameTH")
    @Expose
    public String nameTH;
    @SerializedName("surnameTH")
    @Expose
    public String surnameTH;
    @SerializedName("employeeID")
    @Expose
    public String employeeID;
    @SerializedName("leaveID")
    @Expose
    public int leaveID;
    @SerializedName("headPerson")
    @Expose
    public String headPerson;
    @SerializedName("leaveDateFrom")
    @Expose
    public String leaveDateFrom;
    @SerializedName("leaveDateTo")
    @Expose
    public String leaveDateTo;
    @SerializedName("leaveNum")
    @Expose
    public int leaveNum;
    @SerializedName("leaveComment")
    @Expose
    public String leaveComment;
    @SerializedName("leaveImg")
    @Expose
    public String leaveImg;
    @SerializedName("nameType")
    @Expose
    public String nameType;
    @SerializedName("nameTypeTH")
    @Expose
    public String nameTypeTH;
    @SerializedName("leaveStatus")
    @Expose
    public int leaveStatus;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("approvalBy")
    @Expose
    public String approvalBy;
    @SerializedName("createDate")
    @Expose
    public String createDate;
    @SerializedName("updateDate")
    @Expose
    public String updateDate;
    @SerializedName("updateTime")
    @Expose
    public String updateTime;


    @Override
    public String toString() {
        return "HistoryItemDao{" +
                "empLeaveID='" + empLeaveID + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", employeeID='" + employeeID + '\'' +
                ", leaveID=" + leaveID +
                ", headPerson='" + headPerson + '\'' +
                ", leaveDateFrom='" + leaveDateFrom + '\'' +
                ", leaveDateTo='" + leaveDateTo + '\'' +
                ", leaveNum=" + leaveNum +
                ", leaveComment='" + leaveComment + '\'' +
                ", leaveImg='" + leaveImg + '\'' +
                ", nameType='" + nameType + '\'' +
                ", leaveStatus=" + leaveStatus +
                ", comment='" + comment + '\'' +
                ", approvalBy='" + approvalBy + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }

    public String getEmpLeaveID() {
        return empLeaveID;
    }

    public void setEmpLeaveID(String empLeaveID) {
        this.empLeaveID = empLeaveID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public int getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(int leaveID) {
        this.leaveID = leaveID;
    }

    public String getHeadPerson() {
        return headPerson;
    }

    public void setHeadPerson(String headPerson) {
        this.headPerson = headPerson;
    }

    public String getLeaveDateFrom() {
        return leaveDateFrom;
    }

    public void setLeaveDateFrom(String leaveDateFrom) {
        this.leaveDateFrom = leaveDateFrom;
    }

    public String getLeaveDateTo() {
        return leaveDateTo;
    }

    public void setLeaveDateTo(String leaveDateTo) {
        this.leaveDateTo = leaveDateTo;
    }

    public int getLeaveNum() {
        return leaveNum;
    }

    public void setLeaveNum(int leaveNum) {
        this.leaveNum = leaveNum;
    }

    public String getLeaveComment() {
        return leaveComment;
    }

    public void setLeaveComment(String leaveComment) {
        this.leaveComment = leaveComment;
    }

    public String getLeaveImg() {
        return leaveImg;
    }

    public void setLeaveImg(String leaveImg) {
        this.leaveImg = leaveImg;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public int getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(int leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getApprovalBy() {
        return approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getSurnameTH() {
        return surnameTH;
    }

    public void setSurnameTH(String surnameTH) {
        this.surnameTH = surnameTH;
    }

    public String getNameTypeTH() {
        return nameTypeTH;
    }

    public void setNameTypeTH(String nameTypeTH) {
        this.nameTypeTH = nameTypeTH;
    }
}
