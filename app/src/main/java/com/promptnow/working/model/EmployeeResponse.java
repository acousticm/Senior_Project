package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 3/23/2017.
 */
public class EmployeeResponse {
    @SerializedName("result")
    private List<Employee> data;

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }

    public class Employee {
        @SerializedName("employeeID")
        private String Id;
        @SerializedName("name")
        private String name;
        @SerializedName("surname")
        private String surname;
        @SerializedName("nickname")
        private String nickname;
        @SerializedName("birthday")
        private String birthday;
        @SerializedName("email")
        private String email;
        @SerializedName("phoneNo1")
        private String phoneNo1;
        @SerializedName("phoneNo2")
        private String phoneNo2;
        @SerializedName("lineID")
        private String lineID;
        @SerializedName("positionName")
        private String position;
        @SerializedName("departmentName")
        private String department;
        @SerializedName("startDate")
        private String startDate;
        @SerializedName("imgProfileURL")
        private String imgProfile;
        @SerializedName("Token")
        private String token;
        @SerializedName("headPerson")
        private String headPersonID;
        @SerializedName("ApproverID")
        private String ApproverID;
        @SerializedName("citizenID")
        private String citizenID;
        @SerializedName("zipcode")
        private String zipcode;
        @SerializedName("amphoe")
        private String amphoe;
        @SerializedName("district")
        private String district;
        @SerializedName("province")
        private String province;
        @SerializedName("username")
        private String username;
        @SerializedName("nameTH")
        private String nameTH;
        @SerializedName("surnameTH")
        private String surnameTH;
        @SerializedName("nicknameTH")
        private String nicknameTH;

        public String getHeadPersonID() {
            return headPersonID;
        }

        public void setHeadPersonID(String headPersonID) {
            this.headPersonID = headPersonID;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            this.Id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNo1() {
            return phoneNo1;
        }

        public void setPhoneNo1(String phoneNo1) {
            this.phoneNo1 = phoneNo1;
        }

        public String getPhoneNo2() {
            return phoneNo2;
        }

        public void setPhoneNo2(String phoneNo2) {
            this.phoneNo2 = phoneNo2;
        }

        public String getLineID() {
            return lineID;
        }

        public void setLineID(String lineID) {
            this.lineID = lineID;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getImgProfile() {
            if(imgProfile == null){
                return "";
            }
            return imgProfile;
        }

        public void setImgProfile(String imgProfile) {
            this.imgProfile = imgProfile;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getApproverID() {
            return ApproverID;
        }

        public void setApproverID(String approverID) {
            ApproverID = approverID;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getCitizenID() {
            return citizenID;
        }

        public void setCitizenID(String citizenID) {
            this.citizenID = citizenID;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAmphoe() {
            return amphoe;
        }

        public void setAmphoe(String amphoe) {
            this.amphoe = amphoe;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNameTH() {
            return nameTH;
        }

        public void setNameTH(String nameTH) {
            this.nameTH = nameTH;
        }

        public String getSurnameTH() {
            return surnameTH;
        }

        public void setSurnameTH(String surnameTH) {
            this.surnameTH = surnameTH;
        }

        public String getNicknameTH() {
            return nicknameTH;
        }

        public void setNicknameTH(String nicknameTH) {
            this.nicknameTH = nicknameTH;
        }
    }
}
