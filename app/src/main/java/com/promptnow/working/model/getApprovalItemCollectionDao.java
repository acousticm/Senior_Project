package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 7/26/2017.
 */

public class getApprovalItemCollectionDao {
    @SerializedName("result")
    private List<getApprovalItemDao> data;

    public List<getApprovalItemDao> getData() {
        return data;
    }

    public void setData(List<getApprovalItemDao> data) {
        this.data = data;
    }
}
