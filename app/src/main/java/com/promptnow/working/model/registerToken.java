package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephy on 6/17/2017.
 */

public class registerToken {
    @SerializedName("success") private int success;
    @SerializedName("message") private String message;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
