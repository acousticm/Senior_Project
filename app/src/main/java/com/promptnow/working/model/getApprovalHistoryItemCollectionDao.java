package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 7/26/2017.
 */

public class getApprovalHistoryItemCollectionDao {
    @SerializedName("result")
    private List<getApprovalHistoryItemDao> data;

    public List<getApprovalHistoryItemDao> getData() {
        return data;
    }

    public void setData(List<getApprovalHistoryItemDao> data) {
        this.data = data;
    }
}
