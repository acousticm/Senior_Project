package com.promptnow.working.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 12/3/2017.
 */

public class GetYearModel {
    @SerializedName("Status")
    @Expose
    public Boolean status;
    @SerializedName("Year")
    @Expose
    public List<Year> year = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Year> getYear() {
        return year;
    }

    public void setYear(List<Year> year) {
        this.year = year;
    }

    public static class Year {

        @SerializedName("Year")
        @Expose
        public String year;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        @Override
        public String toString() {
            return "Year{" +
                    "year=" + year +
                    '}';
        }
    }
}