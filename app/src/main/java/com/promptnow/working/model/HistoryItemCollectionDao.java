package com.promptnow.working.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 7/8/2017.
 */

public class HistoryItemCollectionDao {
    @SerializedName("result")
    private List<HistoryItemDao> data;

    public List<HistoryItemDao> getData() {
        return data;
    }

    public void setData(List<HistoryItemDao> data) {
        this.data = data;
    }
}
