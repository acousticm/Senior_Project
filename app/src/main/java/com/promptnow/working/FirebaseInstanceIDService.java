package com.promptnow.working;

import android.content.Context;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.registerToken;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Zephy on 6/17/2017.
 */

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        registerToken(token);
    }

    private void registerToken(String token) {
        Call<registerToken> call = HttpManager.getInstance().getService().addToken(Singleton.getInstance().empId,token);
                call.enqueue(new Callback<registerToken>() {
                    @Override
                    public void onResponse(Call<registerToken> call, Response<registerToken> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getApplicationContext(), "Token Found", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<registerToken> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Sad : (", Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }
}
