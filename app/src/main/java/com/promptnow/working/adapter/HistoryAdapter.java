package com.promptnow.working.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.promptnow.working.R;
import com.promptnow.working.activity.HistoryDetailActivity;
import com.promptnow.working.model.HistoryItemDao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zephy on 7/8/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private static final String TAG = "";
    // Call Variable
    private Context context;
    public int LEAVE_CANCEL_STATUS = 3;
    //List to Store all list from API
    public ArrayList<HistoryItemDao> results;
    public ArrayList<HistoryItemDao> originalList;
    CallbackData listener;

    public HistoryAdapter(ArrayList<HistoryItemDao> results, Context context, CallbackData callback) {
        super();
        this.originalList = results;
        this.results = results;
        this.context = context;
        this.listener = callback;
    }

    public interface CallbackData {
        void SendData(boolean status);
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_history, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder holder, final int position) {
        final String empLeaveID = results.get(position).getEmpLeaveID();
        final String employeeID = results.get(position).getEmployeeID();
        final String name = results.get(position).getName();
        final String surname = results.get(position).getSurname();
        final int leaveID = results.get(position).getLeaveID();
        final String headPerson = results.get(position).getHeadPerson();
        final String dateStart = results.get(position).getLeaveDateFrom();
        final String dateEnd = results.get(position).getLeaveDateTo();
        final String d1 = dateThai(dateStart);
        final String d2 = dateThai(dateEnd);
        final int leaveNum = results.get(position).getLeaveNum();
        final String leaveComment = results.get(position).getLeaveComment();
        final String leaveImg = results.get(position).getLeaveImg();
        final String nameType = results.get(position).getNameType();
        final int status = results.get(position).getLeaveStatus();
        final String comment = results.get(position).getComment();
        final String approvalBy = results.get(position).getApprovalBy();
        final String createOn = results.get(position).getCreateDate();
        final String updateDate = results.get(position).getUpdateDate();
        final String updateTime = results.get(position).getUpdateTime();
        final String updateDate2 = dateThai(updateDate);
        final String updateTime2 = timeConvert(updateTime);

        final int pos = position;
        String templeavedatefrom = context.getString(R.string.date);
        String templeavetype = context.getString(R.string.leaveType);
        String tempstatusapprove = context.getString(R.string.statusapprove);
        String tempstatusreject = context.getString(R.string.statusreject);
        String tempstatuspending = context.getString(R.string.statuspending);
        String statustext = context.getString(R.string.status);
        if (d1.equals(d2)) {
            holder.tvDateStart.setText(d1);
            holder.totext.setVisibility(View.GONE);
            holder.tvDateEnd.setVisibility(View.GONE);
        } else {
            holder.tvDateStart.setText(d1);
            holder.tvDateEnd.setText(d2);
        }
        holder.tvLeaveType.setText(nameType);
        if (status == 0) {
            holder.tvStatusApprove.setTextColor(Color.YELLOW);
            holder.tvStatusApprove.setText(tempstatuspending);
        } else if (status == 1) {
            holder.tvStatusApprove.setTextColor(Color.GREEN);
            holder.tvStatusApprove.setText(tempstatusapprove);
        } else if (status == 2) {
            holder.tvStatusApprove.setTextColor(context.getResources().getColor(R.color.orange));
            holder.tvStatusApprove.setText(tempstatusreject);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),
                        HistoryDetailActivity.class);
                intent.putExtra("employeeID", employeeID);
                intent.putExtra("empLeaveID", empLeaveID);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("DateFrom", d1);
                intent.putExtra("DateTo", d2);
                intent.putExtra("leaveNum", leaveNum);
                intent.putExtra("leaveComment", leaveComment);
                intent.putExtra("leaveImg", leaveImg);
                intent.putExtra("nameType", nameType);
                intent.putExtra("leaveID", leaveID);
                intent.putExtra("headPerson", headPerson);
                intent.putExtra("comment", comment);
                intent.putExtra("approvalBy", approvalBy);
                intent.putExtra("createOn", createOn);
                intent.putExtra("leaveStatus", status);
                intent.putExtra("pos", pos);
                intent.putExtra("updateDate", updateDate2);
                intent.putExtra("updateTime", updateTime2);
                context.startActivity(intent);
            }
        });
    }

    public String timeConvert(String time) {
        Resources res = context.getResources();
        String lang = res.getString(R.string.language);
        DateFormat format = new SimpleDateFormat("hh:mm:ss");
        try {
            Date date = format.parse(time);
            SimpleDateFormat format2 = new SimpleDateFormat("hh:mm a");
            String timeresult = format2.format(date);
            if (!lang.equals("EN")) {
                return time;
            } else
                return timeresult;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String dateThai(String strDate) {
        Resources res = context.getResources();
        String Months[] = res.getStringArray(R.array.months_array);
        String lang = res.getString(R.string.language);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        int year = 0, month = 0, day = 0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!lang.equals("EN")) {
            return String.format("%s %s %s", day, Months[month], year + 543);
        } else
            return String.format("%s %s %s", day, Months[month], year);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if (results.size() <= 0)
            return 0;
        return results.size();
    }

    public void filterData(String query, String year) {
        ArrayList<HistoryItemDao> newList = new ArrayList<HistoryItemDao>();
        if (query.length() == 0 && year.length() == 0) {
            newList.addAll(originalList);
            listener.SendData(true);
        } else {
            for (HistoryItemDao item : originalList) {
                if (item.getNameType().contains(query) && item.getLeaveDateFrom().contains(year)) { // Found Item
                    newList.add(item);
                }
            }
            if (newList.size() <= 0) {
                listener.SendData(false);
            } else {
                listener.SendData(true);
            }
        }
        results = newList;
        notifyDataSetChanged();
    }

    public void loadData() {
        results = originalList;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDateStart, tvDateEnd, tvStatusApprove, tvLeaveType, totext;

        public ViewHolder(final View itemView) {
            super(itemView);

            tvDateStart = (TextView) itemView.findViewById(R.id.tvDateStart);
            tvDateEnd = (TextView) itemView.findViewById(R.id.tvDateEnd);
            tvStatusApprove = (TextView) itemView.findViewById(R.id.tvStatusApprove);
            tvLeaveType = (TextView) itemView.findViewById(R.id.tvLeaveType);
            totext = (TextView) itemView.findViewById(R.id.totext);
        }
    }

}
