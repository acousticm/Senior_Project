package com.promptnow.working.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.promptnow.working.R;
import com.promptnow.working.model.LeaveType;

import java.util.ArrayList;

/**
 * Created by Zephy on 4/4/2017.
 */

public class SpinnerAdapter extends BaseAdapter {
    private ArrayList<LeaveType.Leavetype> lstData;
    private Context context;
    private LayoutInflater inflater;
    int id;

    public SpinnerAdapter(ArrayList<LeaveType.Leavetype> lstData, Context context) {
        this.lstData = lstData;
        this.context = context;
        this.inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lstData.size();
    }

    @Override
    public Object getItem(int position) {
        return lstData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)
            view = inflater.inflate(R.layout.spinner_layout, parent, false);
        TextView tv = (TextView) view.findViewById(R.id.txt);
        tv.setText(lstData.get(position).getNameType());
        id = lstData.get(position).getLeaveID();
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        LinearLayout ll = (LinearLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.txt);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Prompt-Light.ttf");
        tv.setTypeface(font);
        tv.setGravity(Gravity.LEFT);
        //tv.setBackgroundResource(R.color.backgroundstd);
        tv.setTextColor(Color.parseColor("#000000"));
        tv.setTextSize(20);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        return view;
    }
}
