package com.promptnow.working.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.promptnow.working.R;
import com.promptnow.working.activity.MoreDetailActivity;
import com.promptnow.working.model.getApprovalItemDao;

import org.joda.time.LocalTime;
import org.w3c.dom.Text;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;


/**
 * Created by Zephy on 7/26/2017.
 */

public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.ViewHolder> {

    private Context context;
    public ArrayList<getApprovalItemDao> results;

    public ApprovalAdapter(ArrayList<getApprovalItemDao> results, Context context) {
        super();
        this.results = results;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_leave_approval, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ApprovalAdapter.ViewHolder holder, final int position) {
        final int leaveID = results.get(position).getLeaveID();
        final String employeeID = results.get(position).getEmployeeId();
        final String empLeaveID = results.get(position).getEmpLeaveID();
        final String name = results.get(position).getName();
        final String surname = results.get(position).getSurname();
        final String DateFrom = results.get(position).getLeaveDateFrom();
        final String DateTo = results.get(position).getLeaveDateTo();
        final String leaveNum = results.get(position).getLeaveNum();
        final String leaveComment = results.get(position).getLeaveComment();
        final String leaveImg = results.get(position).getLeaveImg();
        final String nameType = results.get(position).getNameType();
        final String comment = results.get(position).getComment();
        final int leaveMax = results.get(position).getLeaveMax();
        final String createDate = results.get(position).getCreateDate();
        final String time = results.get(position).getTime();
        final String condatestart = dateThai(DateFrom);
        final String condateend = dateThai(DateTo);
        String timeresult = timeConvert(time);

        holder.tvFrom.setText(name + " " + surname);
        holder.textetc.setText(dateThai(createDate));
        holder.textTime.setText(timeresult);
        holder.tvLeaveType.setText(nameType);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),
                        MoreDetailActivity.class);
                intent.putExtra("employeeID", employeeID);
                intent.putExtra("empLeaveID", empLeaveID);
                intent.putExtra("leaveID", leaveID);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("DateFrom", condatestart);
                intent.putExtra("DateTo", condateend);
                intent.putExtra("leaveNum", leaveNum);
                intent.putExtra("leaveComment", leaveComment);
                intent.putExtra("leaveImg", leaveImg);
                intent.putExtra("nameType", nameType);
                intent.putExtra("leaveMax", leaveMax);
                context.startActivity(intent);
            }
        });

    }

    public String timeConvert(String time) {
        DateFormat format = new SimpleDateFormat("hh:mm:ss");
        try {
            Date date = format.parse(time);
            SimpleDateFormat format2 = new SimpleDateFormat("hh:mm a");
            String timeresult = format2.format(date);
            if (!context.getString(R.string.language).equals("EN")) {
                return time;
            } else
                return timeresult;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String dateThai(String strDate) {
        Resources res = context.getResources();
        String lang = res.getString(R.string.language);
        String Months[] = res.getStringArray(R.array.months_array);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        int year = 0, month = 0, day = 0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!lang.equals("EN")) {
            return String.format("%s %s %s", day, Months[month], year + 543);
        } else
            return String.format("%s %s %s", day, Months[month], year);
    }

    @Override
    public int getItemCount() {
        if (results.size() <= 0)
            return 0;
        return results.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFrom, textetc, textTime, tvLeaveType;

        public ViewHolder(final View itemView) {
            super(itemView);
            tvFrom = (TextView) itemView.findViewById(R.id.tvFrom);
            textetc = (TextView) itemView.findViewById(R.id.textetc);
            textTime = (TextView) itemView.findViewById(R.id.textTime);
            tvLeaveType = (TextView) itemView.findViewById(R.id.tvLeaveType);
        }
    }

}
