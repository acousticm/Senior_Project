package com.promptnow.working.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.promptnow.working.R;
import com.promptnow.working.activity.HistoryApprovalActivity;
import com.promptnow.working.activity.MoreDetailActivity;
import com.promptnow.working.model.getApprovalHistoryItemDao;
import com.promptnow.working.model.getApprovalItemDao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Zephy on 7/26/2017.
 */

public class ApprovalHistoryAdapter extends RecyclerView.Adapter<ApprovalHistoryAdapter.ViewHolder> {

    private static final String TAG = "";
    private Context context;
    public ArrayList<getApprovalHistoryItemDao> results;

    public ApprovalHistoryAdapter(ArrayList<getApprovalHistoryItemDao> results, Context context) {
        super();
        this.results = results;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_history, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ApprovalHistoryAdapter.ViewHolder holder, final int position) {
        final int leaveID = results.get(position).getLeaveID();
        final String employeeID = results.get(position).getEmployeeId();
        final String empLeaveID = results.get(position).getEmpLeaveID();
        final String name = results.get(position).getName();
        final String surname = results.get(position).getSurname();
        final String DateFrom = results.get(position).getLeaveDateFrom();
        final String DateTo = results.get(position).getLeaveDateTo();
        final int leaveNum = results.get(position).getLeaveNum();
        final String leaveComment = results.get(position).getLeaveComment();
        final String leaveImg = results.get(position).getLeaveImg();
        final String nameType = results.get(position).getNameType();
        final int status = results.get(position).getLeaveStatus();
        final String comment = results.get(position).getComment();
        final int leaveMax = results.get(position).getLeaveMax();
        final String condatestart = dateThai(DateFrom);
        final String condateend = dateThai(DateTo);
        final String approveBy = results.get(position).getApproveBy();
        final String updateDate = results.get(position).getUpdateDate();
        final String updateTime = results.get(position).getUpdateTime();
        final String updateDate2 = dateThai(updateDate);
        final String updateTime2 = timeConvert(updateTime);

        String templeavedatefrom = context.getString(R.string.date);
        String templeavetype = context.getString(R.string.leaveType);
        String tempstatusapprove = context.getString(R.string.statusapprove);
        String tempstatusreject = context.getString(R.string.statusreject);
        String statustext = context.getString(R.string.status);

        if (condatestart.equals(condateend)) {
            holder.tvDateStart.setText(condatestart);
            holder.totext.setVisibility(View.GONE);
            holder.tvDateEnd.setVisibility(View.GONE);
        } else {
            holder.tvDateStart.setText(condatestart);
            holder.tvDateEnd.setText(condateend);
        }
        // Name Surname
        holder.tvLeaveType.setText(name + " " + surname);
        if (status == 1) {
            holder.tvStatusApprove.setTextColor(Color.GREEN);
            holder.tvStatusApprove.setText(tempstatusapprove);
        } else if (status == 2) {
            holder.tvStatusApprove.setTextColor(context.getResources().getColor(R.color.orange));
            holder.tvStatusApprove.setText(tempstatusreject);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),
                        HistoryApprovalActivity.class);
                intent.putExtra("employeeID", employeeID);
                intent.putExtra("empLeaveID", empLeaveID);
                intent.putExtra("leaveID", leaveID);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("DateFrom", condatestart);
                intent.putExtra("leaveStatus", status);
                intent.putExtra("DateTo", condateend);
                intent.putExtra("leaveNum", leaveNum);
                intent.putExtra("leaveComment", leaveComment);
                intent.putExtra("leaveImg", leaveImg);
                intent.putExtra("leaveImg", leaveImg);
                intent.putExtra("nameType", nameType);
                intent.putExtra("comment", comment);
                intent.putExtra("approveBy", approveBy);
                intent.putExtra("leaveMax", leaveMax);
                intent.putExtra("updateDate", updateDate2);
                intent.putExtra("updateTime", updateTime2);
                context.startActivity(intent);
            }
        });

    }

    public String timeConvert(String time) {
        Resources res = context.getResources();
        String lang = res.getString(R.string.language);
        DateFormat format = new SimpleDateFormat("hh:mm:ss");
        try {
            Date date = format.parse(time);
            SimpleDateFormat format2 = new SimpleDateFormat("hh:mm a");
            String timeresult = format2.format(date);
            if (!lang.equals("EN")) {
                return time;
            } else
                return timeresult;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String dateThai(String strDate) {
        Resources res = context.getResources();
        String Months[] = res.getStringArray(R.array.months_array);
        String lang = res.getString(R.string.language);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        int year = 0, month = 0, day = 0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!lang.equals("EN")) {
            return String.format("%s %s %s", day, Months[month], year + 543);
        } else
            return String.format("%s %s %s", day, Months[month], year);
    }

    @Override
    public int getItemCount() {
        if (results.size() <= 0)
            return 0;
        return results.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDateStart, tvDateEnd, tvStatusApprove, tvLeaveType, totext;

        public ViewHolder(final View itemView) {
            super(itemView);
            totext = (TextView) itemView.findViewById(R.id.totext);
            tvDateStart = (TextView) itemView.findViewById(R.id.tvDateStart);
            tvDateEnd = (TextView) itemView.findViewById(R.id.tvDateEnd);
            tvStatusApprove = (TextView) itemView.findViewById(R.id.tvStatusApprove);
            tvLeaveType = (TextView) itemView.findViewById(R.id.tvLeaveType);
        }
    }

}
