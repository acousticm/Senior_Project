package com.promptnow.working.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.promptnow.working.R;
import com.promptnow.working.model.GetYearModel;

import java.util.ArrayList;

/**
 * Created by Zephy on 12/3/2017.
 */

public class SpinnerYearAdapter extends BaseAdapter {
    private ArrayList<GetYearModel.Year> list;
    private Context context;
    private LayoutInflater inflater;

    public SpinnerYearAdapter(ArrayList<GetYearModel.Year> lstData, Context context) {
        this.list = lstData;
        this.context = context;
        this.inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(list.size() <= 0)
            return 0;
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)
            view = inflater.inflate(R.layout.spinneryear, parent, false);
        TextView tv = (TextView) view.findViewById(R.id.txt);
        tv.setText(list.get(position).getYear());
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        LinearLayout ll = (LinearLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.txt);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Prompt-Light.ttf");
        tv.setTypeface(font);
        tv.setGravity(Gravity.LEFT);
        tv.setTextColor(Color.parseColor("#000000"));
        tv.setTextSize(20);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        return view;
    }
}
