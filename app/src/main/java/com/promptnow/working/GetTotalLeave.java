package com.promptnow.working;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.Graph;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by Zephy on 8/15/2017.
 */

public class GetTotalLeave {

    int leaveId, leaveMax, total;
    String nameType;

    public int getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    public int getLeaveMax() {
        return leaveMax;
    }

    public void setLeaveMax(int leaveMax) {
        this.leaveMax = leaveMax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public void getTotalLeave(String empId, int leaveId){
        Call<Graph> call = HttpManager.getInstance().getService().getTotalLeave(empId);
        call.enqueue(new Callback<Graph>() {
            @Override
            public void onResponse(Call<Graph> call, Response<Graph> response) {
                    setLeaveId(response.body().getGraph().get(0).getLeaveID());
                    setNameType((String) response.body().getGraph().get(0).getNameType());
                    setTotal(response.body().getGraph().get(0).getTotal());
                    setLeaveMax(response.body().getGraph().get(0).getLeaveMax());
            }

            @Override
            public void onFailure(Call<Graph> call, Throwable t) {

            }
        });
    }
}
