package com.promptnow.working;

/**
 * Created by Zephy on 3/28/2017.
 */

public class Singleton {

    private static Singleton instance;
    String empId;
    String Token;
    String headPersonID;
    int position;
    String fullname;
    public int HolidayMax, HolidayTotal, PersonTotal, MedicineTotal;

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public String getHeadPersonID() {
        return headPersonID;
    }

    public void setHeadPersonID(String headPersonID) {
        this.headPersonID = headPersonID;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getHolidayMax() {
        return HolidayMax;
    }

    public void setHolidayMax(int holidayMax) {
        HolidayMax = holidayMax;
    }

    public int getHolidayTotal() {
        return HolidayTotal;
    }

    public void setHolidayTotal(int holidayTotal) {
        HolidayTotal = holidayTotal;
    }

    public int getPersonTotal() {
        return PersonTotal;
    }

    public void setPersonTotal(int personTotal) {
        PersonTotal = personTotal;
    }

    public int getMedicineTotal() {
        return MedicineTotal;
    }

    public void setMedicineTotal(int medicineTotal) {
        MedicineTotal = medicineTotal;
    }
}
