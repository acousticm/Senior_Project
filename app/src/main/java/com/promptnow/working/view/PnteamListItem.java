package com.promptnow.working.view;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;
import com.inthecheesefactory.thecheeselibrary.view.state.BundleSavedState;
import com.promptnow.working.R;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PnteamListItem extends BaseCustomViewGroup {
    TextView expEmail;
    TextView expPhone;
    TextView expPhone2;
    TextView expLineId;

    public PnteamListItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public PnteamListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public PnteamListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public PnteamListItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.list_name_pnteam, this);
    }

    private void initInstances() {
        // findViewById here
        expEmail = (TextView) findViewById(R.id.expEmail);
        expPhone = (TextView) findViewById(R.id.expPhone);
        expPhone2 = (TextView) findViewById(R.id.expPhone2);
        expLineId = (TextView) findViewById(R.id.expLineId);

        expEmail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = "mailto:" + expEmail.getText().toString();
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(email));
                try {
                    getContext().startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {

                }
            }
        });

        expPhone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = expPhone.getText().toString();
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                getContext().startActivity(phoneIntent);
            }
        });

        expPhone2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone2 = expPhone2.getText().toString();
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone2, null));
                getContext().startActivity(phoneIntent);
            }
        });
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        BundleSavedState savedState = new BundleSavedState(superState);
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);

        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        BundleSavedState ss = (BundleSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        Bundle bundle = ss.getBundle();
        // Restore State from bundle here
    }

    public void setExpEmail(String text) {
        expEmail.setText(text);
    }

    public void setExpPhone(String text) {
        expPhone.setText(text);
    }

    public void setExpPhone2(String text) {
        expPhone2.setText(text);
    }

    public void setExpLineId(String text) {
        expLineId.setText(text);
    }
}
