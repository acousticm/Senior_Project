package com.promptnow.working.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.promptnow.working.R;


/**
 * Created by Nanthakorn on 10/10/2016.
 */

public class MenuView extends LinearLayout {
    private Context context;
    private RelativeLayout layout;
    private LinearLayout mainLayout;
    private TintableImageView imgMenuIcon;
    private TextView tvMenuTitle;
    private FrameLayout activeView;

    public MenuView(Context context) {
        super(context);
        this.context = context;
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(context, attrs, 0);
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyle){
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuView, 0, 0);

        layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.view_menu, null);
        mainLayout = (LinearLayout) layout.findViewById(R.id.mainLayout);
        imgMenuIcon = (TintableImageView) layout.findViewById(R.id.imgMenuIcon);
        tvMenuTitle = (TextView) layout.findViewById(R.id.tvMenuTitle);
        activeView = (FrameLayout) layout.findViewById(R.id.activeView);

        try {
            int icon = a.getResourceId(R.styleable.MenuView_menuIcon, 0);
            imgMenuIcon.setImageResource(icon);
            String title = a.getString(R.styleable.MenuView_menuTitle);
            tvMenuTitle.setText(title);
            boolean active = a.getBoolean(R.styleable.MenuView_menuActive, false);
            setActive(active);
        } finally {
            a.recycle();
        }

        addView(layout,  new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public void setActive(boolean active){
        if (active){
            activeView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            mainLayout.setBackgroundColor(0);
        }else {
            activeView.setBackgroundColor(0);
            mainLayout.setBackgroundColor(0);
        }
    }
}
