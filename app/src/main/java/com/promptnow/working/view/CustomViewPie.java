package com.promptnow.working.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.Toast;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;
import com.inthecheesefactory.thecheeselibrary.view.state.BundleSavedState;
import com.promptnow.working.R;

/**
 * Created by Zephy on 4/26/2017.
 */
public class CustomViewPie extends BaseCustomViewGroup {
    private TextView tvText;
    private DecoView tvPie;

    public CustomViewPie(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public CustomViewPie(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public CustomViewPie(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public CustomViewPie(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.layout_pie, this);
        initInstances();
    }

    private void initInstances() {
        // findViewById here
        tvText = (TextView) findViewById(R.id.tvText);
        tvPie = (DecoView) findViewById(R.id.tvPie);
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    public void setTvText(String text) {
        tvText.setText(text);
    }

    public void setPie(float total, float max) {
        float density = getResources().getDisplayMetrics().density;
        if (density <= 1.5) {
            tvPie.addSeries(new SeriesItem.Builder(Color.argb(255, 38, 50, 56))
                    .setRange(0, 100, 100)
                    .setInitialVisibility(false)
                    .setLineWidth(10f)
                    .setShadowSize(1)
                    .build());

            tvPie.configureAngles(270, 0);
            if (total <= max) {
                total = (total / max) * 100;
                total = 100 - total;
                SeriesItem seriesItem1 = new SeriesItem.Builder(Color.argb(255, 51, 102, 204), Color.argb(255, 255, 255, 255))
                        .setRange(0, 100, 0)
                        .setLineWidth(15f)
                        .build();
                int series1Index = tvPie.addSeries(seriesItem1);
                tvPie.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                        .setDelay(100)
                        .setDuration(200)
                        .build());
                tvPie.addEvent(new DecoEvent.Builder(total).setIndex(series1Index).setDelay(200).build());
            }
        } else {
            tvPie.addSeries(new SeriesItem.Builder(Color.argb(255, 38, 50, 56))
                    .setRange(0, 100, 100)
                    .setInitialVisibility(false)
                    .setLineWidth(15f)
                    .setShadowSize(1)
                    .build());

            tvPie.configureAngles(270, 0);
            if (total <= max) {
                total = (total / max) * 100;
                total = 100 - total;
                SeriesItem seriesItem1 = new SeriesItem.Builder(Color.argb(255, 51, 102, 204), Color.argb(255, 255, 255, 255))
                        .setRange(0, 100, 0)
                        .setLineWidth(23f)
                        .build();
                int series1Index = tvPie.addSeries(seriesItem1);
                tvPie.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                        .setDelay(100)
                        .setDuration(200)
                        .build());
                tvPie.addEvent(new DecoEvent.Builder(total).setIndex(series1Index).setDelay(200).build());
            }
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        BundleSavedState savedState = new BundleSavedState(superState);
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);

        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        BundleSavedState ss = (BundleSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        Bundle bundle = ss.getBundle();
        // Restore State from bundle here
    }

}
