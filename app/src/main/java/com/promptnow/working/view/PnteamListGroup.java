package com.promptnow.working.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;
import com.inthecheesefactory.thecheeselibrary.view.state.BundleSavedState;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.promptnow.working.R;
import com.squareup.picasso.Picasso;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PnteamListGroup extends BaseCustomViewGroup {
    LinearLayout boxdetail;
    CircularImageView tvempPic;
    TextView tvfullName;
    TextView tvId;
    TextView tvjobPosition;
    TextView tvjobDepart;
    TextView tvnickname;
    ImageView tvIndicator;

    public PnteamListGroup(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public PnteamListGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public PnteamListGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public PnteamListGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.list_group_pnteam, this);
    }

    private void initInstances() {
        // findViewById here
        tvempPic = (CircularImageView) findViewById(R.id.tvempPic);
        tvfullName = (TextView) findViewById(R.id.tvfullName);
        tvjobPosition = (TextView) findViewById(R.id.tvjobPosition);
        tvjobDepart = (TextView) findViewById(R.id.tvjobDepartment);
        boxdetail = (LinearLayout) findViewById(R.id.boxdetail);
        tvIndicator = (ImageView) findViewById(R.id.indicatoricon);
        tvnickname = (TextView) findViewById(R.id.tvnickname);
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        BundleSavedState savedState = new BundleSavedState(superState);
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);

        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        BundleSavedState ss = (BundleSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        Bundle bundle = ss.getBundle();
        // Restore State from bundle here
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec); // width in px
        int height = width * 2 / 7;
        int newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(
                height,
                MeasureSpec.EXACTLY
        );
        // Child Views
        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec);
        // Self
        setMeasuredDimension(width, height);
    }


    public void setTvjobDepart(String text) {
        tvjobDepart.setText(text);
    }

    public void setFullNameText(String text) {
        tvfullName.setText(text);
    }

    public void setTvIndicator(boolean isExpand) {
        if (isExpand) {
            tvIndicator.setImageResource(R.drawable.up_arrow);
        } else {
            tvIndicator.setImageResource(R.drawable.down_arrow);
        }
    }

    public void setTvnickname(String text) {
        tvnickname.setText(text);
    }

    public void setJobPositionText(String text) {
        tvjobPosition.setText(text);
    }

    public void setImageUrl(String url) {
        Glide.with(getContext())
                .load(url)
                .asBitmap()
                .placeholder(R.drawable.profileimg)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(tvempPic);
    }

    public void setImageUrl2(Bitmap url) {
        tvempPic.setImageBitmap(url);
    }


    @Override
    public String toString() {
        return "PnteamListGroup{" +
                "tvempPic=" + tvempPic +
                ", tvfullName=" + tvfullName +
                ", tvId=" + tvId +
                ", tvjobPosition=" + tvjobPosition +
                '}';
    }
}
