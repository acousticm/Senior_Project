package com.promptnow.working;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zephy on 8/15/2017.
 */

public class HolidayCalculator {

    private static HolidayCalculator instance;
    int MaxHolidayLeave;
    String TAG = "";

    public static HolidayCalculator getInstance() {
        if (instance == null) {
            instance = new HolidayCalculator();
        }
        return instance;
    }

    public int getMaxHolidayLeave() {
        return MaxHolidayLeave;
    }

    public void setMaxHolidayLeave(int maxHolidayLeave) {
        this.MaxHolidayLeave = maxHolidayLeave;
    }

    public int CalculateHoliday(String d1) {
        try {
            Log.d(TAG, "CalculateHoliday: "+d1);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(d1);

            // Start Date
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            int year1 = cal.get(Calendar.YEAR);
            // Today
            Calendar calNow = Calendar.getInstance();
            int year2 = calNow.get(Calendar.YEAR);
            int total = year2 - year1;

            Log.d(TAG, "CalculateHoliday: " + year2 + " " + year1);
            if (total == 0) {
                setMaxHolidayLeave(0);
            }
            if (total == 1) {
                Log.d(TAG, "CalculateHoliday: total == 1");
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) >= cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(6);
                }
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) < cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(0);
                }
                if (calNow.get(Calendar.MONTH) < cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(0);
                }
                if (calNow.get(Calendar.MONTH) > cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(6);
                }
            }
            if (total == 2) {
                Log.d(TAG, "CalculateHoliday: total == 2");
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) >= cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(8);
                }
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) < cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(6);
                }
                if (calNow.get(Calendar.MONTH) < cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(6);
                }
                if (calNow.get(Calendar.MONTH) > cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(6);
                }
            }
            if (total >= 3) {
                Log.d(TAG, "CalculateHoliday: total == 3");
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) >= cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(8);
                }
                if (calNow.get(Calendar.MONTH) == cal.get(Calendar.MONTH) &&
                        calNow.get(Calendar.DATE) < cal.get(Calendar.DATE)) {
                    setMaxHolidayLeave(6);
                }
                if (calNow.get(Calendar.MONTH) < cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(6);
                }
                if (calNow.get(Calendar.MONTH) > cal.get(Calendar.MONTH)) {
                    setMaxHolidayLeave(8);
                }
            }
        } catch (ParseException ex) {
            Log.d(TAG, "CalculateHoliday: "+ex.toString());
        }
        return MaxHolidayLeave;
    }
}
