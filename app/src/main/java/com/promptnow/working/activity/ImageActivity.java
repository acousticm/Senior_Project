package com.promptnow.working.activity;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.promptnow.working.R;


public class ImageActivity extends AppCompatActivity {

    PhotoView image;
    boolean isImageFitToScreen;
    String ImageUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        init();
    }

    private void init() {
        image = (PhotoView) findViewById(R.id.Image);
        Bundle bundle = this.getIntent().getExtras();
        ImageUrl = bundle.getString("leaveImg");
        try {
            Glide.with(this)
                    .load(ImageUrl)
                    .asBitmap()
                    .into(image);
        } catch (IllegalArgumentException e) {
            e.toString();
        }
    }

}