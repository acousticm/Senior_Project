package com.promptnow.working.activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.Singleton;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.MSG;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Zephy on 3/10/2017.
 */

public class LoginActivity extends LocalizationActivity implements View.OnClickListener {

    private Button btnlogin;
    private EditText username, password;
    private Session session;
    private String empId;
    private String Token;
    private TextView btn_th, btn_en;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initTranparentStatusBar();

        session = new Session(this);
        btnlogin = (Button) findViewById(R.id.btnSignUp);
        username = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        btn_th = (TextView) findViewById(R.id.btn_th);
        btn_en = (TextView) findViewById(R.id.btn_en);
        btn_en.setOnClickListener(this);
        btn_th.setOnClickListener(this);
        btnlogin.setOnClickListener(this);

        if (session.loggedin()) {
            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void initTranparentStatusBar() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                if (username.getText().toString().equals("") || password.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this,
                            "Please Insert Username and Password",
                            Toast.LENGTH_SHORT)
                            .show();
                } else {
                    login();
                }
                break;
            case R.id.btn_th:
                setLanguage("th");
                break;
            case R.id.btn_en:
                setLanguage("en");
                break;
            default:
        }
    }


    private void login() {
        String email = username.getText().toString();
        String pwd = password.getText().toString();
        Call<MSG> call = HttpManager.getInstance().getService().userLogIn(email, pwd);
        call.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                if (response.body().getResult().getSuccess() == 1) {
                    empId = response.body().getResult().getEmployeeID();
                    Token = response.body().getResult().getToken();
                    Singleton.getInstance().setEmpId(empId);
                    Singleton.getInstance().setToken(Token);
                    session.setLoggedin(true);
                    session.setEmployee(empId);
                    session.setToken(Token);
                    startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Toast.makeText(LoginActivity.this, "Wrong Username or Password!", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                Log.i("Test", "onFailure: " + t.toString());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
