package com.promptnow.working.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.Singleton;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.PushNotification;
import com.promptnow.working.model.UpdateModel;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Zephy on 7/27/2017.
 */

public class MoreDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "";
    String name, surname, employeeID, empLeaveID, dateFrom, dateTo, leaveNum,
            leaveComment, leaveImg, nameType, leaveMax;
    int leaveId;
    TextView tvFullname, tvempId, tvleaveType, tvDateFrom, tvDateTo, tvTotal, tvComment;
    EditText tvReason;
    Button btnShowMedicine, btnApproval, btnReject;
    final int LEAVE_APPROVE = 1, LEAVE_REJECT = 2;
    private Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_detail);
        Bundle bundle = this.getIntent().getExtras();
        leaveId = bundle.getInt("leaveID");
        employeeID = bundle.getString("employeeID");
        empLeaveID = bundle.getString("empLeaveID");
        name = bundle.getString("name");
        surname = bundle.getString("surname");
        dateFrom = bundle.getString("DateFrom");
        dateTo = bundle.getString("DateTo");
        leaveNum = bundle.getString("leaveNum");
        leaveComment = bundle.getString("leaveComment");
        leaveImg = bundle.getString("leaveImg");
        nameType = bundle.getString("nameType");
        leaveMax = bundle.getString("leaveMax");
        session = new Session(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initInstances();

        if (leaveId == 1 && leaveImg.equals("")) {
            btnShowMedicine.setVisibility(View.GONE);
        } else if(leaveId == 1 && !leaveImg.equals("")) {
            btnShowMedicine.setVisibility(View.VISIBLE);
        }else {
            btnShowMedicine.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstances() {
        tvFullname = (TextView) findViewById(R.id.tvfullname);
        tvleaveType = (TextView) findViewById(R.id.tvLeaveType);
        tvDateFrom = (TextView) findViewById(R.id.tvDateFrom);
        tvDateTo = (TextView) findViewById(R.id.tvDateTo);
        tvTotal = (TextView) findViewById(R.id.tvTotalLeave);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvReason = (EditText) findViewById(R.id.tvReason);
        btnShowMedicine = (Button) findViewById(R.id.btnShowMedicine);
        btnApproval = (Button) findViewById(R.id.btnApproval);
        btnReject = (Button) findViewById(R.id.btnReject);
        btnShowMedicine.setOnClickListener(this);
        btnApproval.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        String tempday = getString(R.string.day);
        tvReason.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // if enter is pressed start calculating
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {

                    // get EditText text
                    String text = ((EditText) v).getText().toString();

                    // find how many rows it cointains
                    int editTextRowCount = text.split("\\n").length;

                    // user has input more than limited - lets do something
                    // about that
                    if (editTextRowCount >= 4) {

                        // find the last break
                        int lastBreakIndex = text.lastIndexOf("\n");

                        // compose new text
                        String newText = text.substring(0, lastBreakIndex);

                        // add new text - delete old one and append new one
                        // (append because I want the cursor to be at the end)
                        ((EditText) v).setText("");
                        ((EditText) v).append(newText);

                    }
                }
                return false;
            }
        });

        tvFullname.setText(name + "  " + surname);
        tvleaveType.setText(nameType);
        tvDateFrom.setText(dateFrom);
        tvDateTo.setText(dateTo);
        tvTotal.setText(leaveNum + " " + tempday);
        tvComment.setText(leaveComment);

    }

    private void AcceptLeave() {
        final String a = getString(R.string.approvetitle);
        final String b = getString(R.string.approvecontent);
        final String c = getString(R.string.textconfirm);
        final String d = getString(R.string.textcancel);
        final SweetAlertDialog ConfirmDialog = new SweetAlertDialog(this,
                SweetAlertDialog.WARNING_TYPE);
        ConfirmDialog.setTitleText(a);
        ConfirmDialog.setContentText(b);
        ConfirmDialog.setConfirmText(c);
        ConfirmDialog.setCancelText(d);
        ConfirmDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Call<UpdateModel> call = HttpManager.getInstance().getService().ApprovalLeave(empLeaveID,
                        LEAVE_APPROVE, tvReason.getText().toString(), session.getEmployeeID());
                call.enqueue(new Callback<UpdateModel>() {
                    @Override
                    public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {
                        if (response.body().getSuccess() == 1) {

                            if(ConfirmDialog.isShowing()){
                                ConfirmDialog.dismissWithAnimation();
                                MoreDetailActivity.super.onBackPressed();
                                //sendNotification();
                            }
                            else {
                                ConfirmDialog.dismissWithAnimation();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateModel> call, Throwable t) {

                    }
                });
            }
        }).show();
    }

    private void RejectLeave() {
        String c = getString(R.string.textconfirm);
        String d = getString(R.string.textcancel);
        String f = getString(R.string.rejecttitle);
        String g = getString(R.string.rejectcontent);
        final SweetAlertDialog RejectDialog = new SweetAlertDialog(this,
                SweetAlertDialog.WARNING_TYPE);
        RejectDialog.setTitleText(f);
        RejectDialog.setContentText(g);
        RejectDialog.setConfirmText(c);
        RejectDialog.setCancelText(d);
        RejectDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Call<UpdateModel> call = HttpManager.getInstance().getService().ApprovalLeave(empLeaveID,
                        LEAVE_REJECT, tvReason.getText().toString(), session.getEmployeeID());
                call.enqueue(new Callback<UpdateModel>() {
                    @Override
                    public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {
                        if (response.body().getSuccess() == 1) {
                            if(RejectDialog.isShowing()){
                                MoreDetailActivity.super.onBackPressed();
                                RejectDialog.dismissWithAnimation();
                                //sendNotification2();
                            }
                            else {
                                RejectDialog.dismissWithAnimation();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateModel> call, Throwable t) {

                    }
                });
            }
        }).show();
    }

    public void sendNotification() {
        Call<PushNotification> call = HttpManager.getInstance().getService().CallPush2("Your Leave Has Been Approval", empLeaveID);
        call.enqueue(new Callback<PushNotification>() {
            @Override
            public void onResponse(Call<PushNotification> call, Response<PushNotification> response) {
                if (response.isSuccessful()) {
                    MoreDetailActivity.super.onBackPressed();
                }

            }

            @Override
            public void onFailure(Call<PushNotification> call, Throwable t) {

            }
        });
    }

    public void sendNotification2() {
        Call<PushNotification> call = HttpManager.getInstance().getService().CallPush2("Your Leave Has Been Rejected", empLeaveID);
        call.enqueue(new Callback<PushNotification>() {
            @Override
            public void onResponse(Call<PushNotification> call, Response<PushNotification> response) {
                if (response.isSuccessful()) {
                    //MoreDetailActivity.super.onBackPressed();
                }

            }

            @Override
            public void onFailure(Call<PushNotification> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShowMedicine:
                ShowImg();
                break;
            case R.id.btnApproval:
                AcceptLeave();
                break;
            case R.id.btnReject:
                RejectLeave();
                break;
        }
    }

    private void ShowImg() {
        Intent intent = new Intent(MoreDetailActivity.this,
                ImageActivity.class);
        intent.putExtra("leaveImg", leaveImg);
        startActivity(intent);
    }
}
