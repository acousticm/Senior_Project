package com.promptnow.working.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.promptnow.working.R;
import com.promptnow.working.databinding.ActivityMainBinding;
import com.promptnow.working.fragment.DashboardFragment;
import com.promptnow.working.fragment.EditProfileFragment;
import com.promptnow.working.fragment.LeaveFragment;
import com.promptnow.working.fragment.PnTeamFragment2;
import com.promptnow.working.view.MenuView;
import com.santalu.emptyview.EmptyView;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DashboardActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    ArrayList<MenuView> menuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initView();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, DashboardFragment.newInstance())
                    .commit();
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        menuList = new ArrayList<>();
        menuList.add(binding.menuDashboard);
        menuList.add(binding.menuProfile);
        menuList.add(binding.menuLeave);
        menuList.add(binding.menuPnTeam);

        for (final MenuView menuView : menuList) {
            menuView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearStateMenu();
                    switch (v.getId()) {
                        case R.id.menuDashboard:
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.contentContainer, new DashboardFragment())
                                    .commit();
                            menuView.setEnabled(false);
                            binding.menuDashboard.setActive(true);
                            break;
                        case R.id.menuProfile:
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.contentContainer, new EditProfileFragment())
                                    .commit();
                            menuView.setEnabled(false);
                            binding.menuProfile.setActive(true);
                            break;
                        case R.id.menuLeave:
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.contentContainer, new LeaveFragment())
                                    .commit();
                            menuView.setEnabled(false);
                            binding.menuLeave.setActive(true);
                            break;
                        case R.id.menuPnTeam:
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.contentContainer, new PnTeamFragment2())
                                    .commit();
                            menuView.setEnabled(false);
                            binding.menuPnTeam.setActive(true);
                            break;
                    }
                }
            });
        }
    }

    private void clearStateMenu() {
        for (MenuView menuView : menuList) {
            menuView.setActive(false);
            menuView.setEnabled(true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
