package com.promptnow.working.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.promptnow.working.R;
import com.promptnow.working.Singleton;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.getApprovalName;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HistoryApprovalActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "";
    String name, surname, employeeID, empLeaveID, dateFrom, dateTo,
            leaveComment, leaveImg, nameType, comment, headPerson,
            approvalBy, createOn, updateDate, updateTime;
    int leaveNum, leaveID, leaveStatus, pos;
    TextView tvStatus2, tvfullname, tvempId, tvLeaveType, tvDateFrom, tvDateTo, tvTotalLeave, tvComment,
            tvApproveby, tvcommentfromhead, tvupdateon;
    Button btnMedic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_history);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle bundle = this.getIntent().getExtras();
        name = bundle.getString("name");
        surname = bundle.getString("surname");
        empLeaveID = bundle.getString("empLeaveID");
        employeeID = bundle.getString("employeeID");
        dateFrom = bundle.getString("DateFrom");
        dateTo = bundle.getString("DateTo");
        leaveNum = bundle.getInt("leaveNum");
        leaveID = bundle.getInt("leaveID");
        leaveStatus = bundle.getInt("leaveStatus");
        nameType = bundle.getString("nameType");
        leaveComment = bundle.getString("leaveComment");
        leaveImg = bundle.getString("leaveImg");
        comment = bundle.getString("comment");
        headPerson = bundle.getString("headPerson");
        approvalBy = bundle.getString("approveBy");
        createOn = bundle.getString("createOn");
        updateDate = bundle.getString("updateDate");
        updateTime = bundle.getString("updateTime");
        pos = bundle.getInt("pos");
        initInstances();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstances() {
        tvStatus2 = (TextView) findViewById(R.id.tvStatus2);
        tvfullname = (TextView) findViewById(R.id.tvfullname);
        tvLeaveType = (TextView) findViewById(R.id.tvLeaveType);
        tvDateFrom = (TextView) findViewById(R.id.tvDateFrom);
        tvDateTo = (TextView) findViewById(R.id.tvDateTo);
        tvTotalLeave = (TextView) findViewById(R.id.tvTotalLeave);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvcommentfromhead = (TextView) findViewById(R.id.commentfromhead);
        tvApproveby = (TextView) findViewById(R.id.tvAprrovedBy);
        tvupdateon = (TextView) findViewById(R.id.tvupdatedon);
        btnMedic = (Button) findViewById(R.id.btnmedic);
        btnMedic.setOnClickListener(this);

        String tempapproved = getString(R.string.approved);
        String temprejected = getString(R.string.rejected);

        if (leaveStatus == 1) {
            tvStatus2.setText(tempapproved);
            tvStatus2.setTextColor(Color.GREEN);
        } else if (leaveStatus == 2) {
            tvStatus2.setText(temprejected);
            tvStatus2.setTextColor(Color.RED);
        }
        if (leaveID == 1 && leaveImg.equals("")) {
            btnMedic.setVisibility(View.GONE);
        } else if (leaveID == 1 && !leaveImg.equals("")) {
            btnMedic.setVisibility(View.VISIBLE);
        } else {
            btnMedic.setVisibility(View.GONE);
        }

        Call<getApprovalName> call = HttpManager.getInstance().getService().getApprovalName(approvalBy);
        call.enqueue(new Callback<getApprovalName>() {
            @Override
            public void onResponse(Call<getApprovalName> call, Response<getApprovalName> response) {
                if (response.isSuccessful()) {
                    if (response.body().result.getSuccess() == 1) {
                        if (getString(R.string.language).equals("EN")) {
                            tvApproveby.setText(response.body().result.getFullname());
                        } else {
                            tvApproveby.setText(response.body().result.getFullnameTH());
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<getApprovalName> call, Throwable t) {

            }
        });

        final String tempday = getString(R.string.day);
        tvfullname.setText(name + "  " + surname);
        tvLeaveType.setText(nameType);
        tvDateFrom.setText(dateFrom);
        tvDateTo.setText(dateTo);
        tvTotalLeave.setText(leaveNum + " " + tempday);
        tvComment.setText(leaveComment);
        tvcommentfromhead.setText(comment);
        tvupdateon.setText(updateDate + " " + updateTime);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnmedic:
                ViewImage();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Singleton.getInstance().setPosition(pos);
        HistoryApprovalActivity.super.onBackPressed();
    }

    private void ViewImage() {
        Intent intent = new Intent(HistoryApprovalActivity.this,
                ImageActivity.class);
        intent.putExtra("leaveImg", leaveImg);
        startActivity(intent);
    }


}
