package com.promptnow.working.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.promptnow.working.R;
import com.promptnow.working.Singleton;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.UpdateModel;
import com.promptnow.working.model.getApprovalName;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "";
    String name, surname, employeeID, empLeaveID, dateFrom, dateTo,
            leaveComment, leaveImg, nameType, comment, headPerson,
            approvalBy, createOn, updateDate, updateTime;
    int leaveNum, leaveID, leaveStatus, pos;
    TextView tvStatus2, tvfullname, tvempId, tvLeaveType, tvDateFrom, tvDateTo, tvTotalLeave, tvComment,
            tvApproveby, tvcommentfromhead, tvupdateon;
    Button btnMedic, btncancel;
    LinearLayout layoutupdateon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Bundle bundle = this.getIntent().getExtras();
        name = bundle.getString("name");
        surname = bundle.getString("surname");
        empLeaveID = bundle.getString("empLeaveID");
        employeeID = bundle.getString("employeeID");
        dateFrom = bundle.getString("DateFrom");
        dateTo = bundle.getString("DateTo");
        leaveNum = bundle.getInt("leaveNum");
        leaveID = bundle.getInt("leaveID");
        leaveStatus = bundle.getInt("leaveStatus");
        nameType = bundle.getString("nameType");
        leaveComment = bundle.getString("leaveComment");
        leaveImg = bundle.getString("leaveImg");
        comment = bundle.getString("comment");
        headPerson = bundle.getString("headPerson");
        approvalBy = bundle.getString("approvalBy");
        createOn = bundle.getString("createOn");
        pos = bundle.getInt("pos");
        updateDate = bundle.getString("updateDate");
        updateTime = bundle.getString("updateTime");
        initInstances();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstances() {
        tvStatus2 = (TextView) findViewById(R.id.tvStatus2);
        tvfullname = (TextView) findViewById(R.id.tvfullname);
        tvLeaveType = (TextView) findViewById(R.id.tvLeaveType);
        tvDateFrom = (TextView) findViewById(R.id.tvDateFrom);
        tvDateTo = (TextView) findViewById(R.id.tvDateTo);
        tvTotalLeave = (TextView) findViewById(R.id.tvTotalLeave);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvcommentfromhead = (TextView) findViewById(R.id.commentfromhead);
        tvApproveby = (TextView) findViewById(R.id.tvAprrovedBy);
        btnMedic = (Button) findViewById(R.id.btnmedic);
        btncancel = (Button) findViewById(R.id.btncancel);
        tvupdateon = (TextView) findViewById(R.id.tvupdateon);
        layoutupdateon = (LinearLayout) findViewById(R.id.layoutupdateon);
        btnMedic.setOnClickListener(this);
        btncancel.setOnClickListener(this);

        String tempapproved = getString(R.string.approved);
        String temprejected = getString(R.string.rejected);
        String temppending = getString(R.string.pending);

        if (leaveStatus == 0) {
            layoutupdateon.setVisibility(View.GONE);
            tvStatus2.setText(temppending);
            tvStatus2.setTextColor(Color.YELLOW);
            tvcommentfromhead.setVisibility(View.GONE);
        } else if (leaveStatus == 1) {
            tvStatus2.setText(tempapproved);
            tvStatus2.setTextColor(Color.GREEN);
            btncancel.setVisibility(View.GONE);
        } else if (leaveStatus == 2) {
            tvStatus2.setText(temprejected);
            tvStatus2.setTextColor(Color.RED);
            btncancel.setVisibility(View.GONE);
        }
        if (leaveID == 1 && leaveImg.equals("")) {
            btnMedic.setVisibility(View.GONE);
        } else if (leaveID == 1 && !leaveImg.equals("")) {
            btnMedic.setVisibility(View.VISIBLE);
        } else {
            btnMedic.setVisibility(View.GONE);
        }

        Call<getApprovalName> call = HttpManager.getInstance().getService().getApprovalName(approvalBy);
        call.enqueue(new Callback<getApprovalName>() {
            @Override
            public void onResponse(Call<getApprovalName> call, Response<getApprovalName> response) {
                if (response.isSuccessful()) {
                    if (response.body().result.getSuccess() == 1) {
                        if (getString(R.string.language).equals("EN")) {
                            tvApproveby.setText(response.body().result.getFullname());
                        } else {
                            tvApproveby.setText(response.body().result.getFullnameTH());
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<getApprovalName> call, Throwable t) {

            }
        });

        final String tempday = getString(R.string.day);
        tvLeaveType.setText(nameType);
        tvDateFrom.setText(dateFrom);
        tvDateTo.setText(dateTo);
        tvTotalLeave.setText(leaveNum + " " + tempday);
        tvComment.setText(leaveComment);
        tvcommentfromhead.setText(comment);
        tvupdateon.setText(updateDate + " " + updateTime);
    }

    @Override
    public void onClick(View v) {
        String a = getString(R.string.textconfirm);
        String b = getString(R.string.textcancel);
        String c = getString(R.string.titleconfirm);
        String d = getString(R.string.contenttextconfirm);
        switch (v.getId()) {
            case R.id.btnmedic:
                ViewImage();
                break;
            case R.id.btncancel:
                final SweetAlertDialog ConfirmDialog = new SweetAlertDialog(HistoryDetailActivity.this,
                        SweetAlertDialog.WARNING_TYPE);
                ConfirmDialog.setTitleText(c);
                ConfirmDialog.setContentText(d);
                ConfirmDialog.setConfirmText(a);
                ConfirmDialog.setCancelText(b);
                ConfirmDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        ConfirmDialog.dismissWithAnimation();
                        cancelLeave(empLeaveID);
                    }
                }).show();
                ConfirmDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        ConfirmDialog.cancel();
                    }
                });
                break;
            default:
        }
    }

    private void cancelLeave(final String empLeaveID) {
        Call<UpdateModel> call = HttpManager.getInstance().getService().deleteLeave(empLeaveID);
        call.enqueue(new Callback<UpdateModel>() {
            @Override
            public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {
                if (response.body().getSuccess() == 1) {
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<UpdateModel> call, Throwable t) {
                Toast.makeText(HistoryDetailActivity.this,
                        t.toString(), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Singleton.getInstance().setPosition(pos);
        HistoryDetailActivity.super.onBackPressed();
    }

    private void ViewImage() {
        Intent intent = new Intent(HistoryDetailActivity.this,
                ImageActivity.class);
        intent.putExtra("leaveImg", leaveImg);
        startActivity(intent);
    }


}
