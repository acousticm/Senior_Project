package com.promptnow.working;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Zephy on 8/11/2017.
 */

public class Session {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context context;
    String empId;
    String Token;
    String headPerson;
    String leaderId;

    public Session(Context context){
        this.context = context;
        prefs = context.getSharedPreferences("myapp", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedin(boolean loggedin){
        editor.putBoolean("loggedInmode", loggedin);
        editor.commit();
    }

    public void setEmployee(String empId){
        editor.putString("empId", empId);
        editor.commit();
    }

    public void setToken(String Token){
        editor.putString("Token", Token);
        editor.commit();
    }

    public String getEmployeeID(){
        empId = prefs.getString("empId", "-1");
        return this.empId;
    }

    public String getToken(){
        Token = prefs.getString("Token", "");
        return this.Token;
    }

    public void setheadPerson(String headPerson){
        editor.putString("headPerson", headPerson);
        editor.commit();
    }

    public String getHeadPerson(){
        headPerson = prefs.getString("headPerson", "");
        return this.headPerson;
    }

    public String getLeaderId() {
        leaderId = prefs.getString("leaderId", "0");
        return this.leaderId;
    }

    public void setLeaderId(String leaderId) {
        editor.putString("leaderId", leaderId);
        editor.commit();
    }

    public boolean loggedin(){
        return prefs.getBoolean("loggedInmode", false);
    }
}
