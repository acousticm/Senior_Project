package com.promptnow.working.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created by Zephy on 6/14/2017.
 */

public class UserManager {
    private final String KEY_PREFS = "prefs_user";
    private final String KEY_ID = "id";
    private final String KEY_USERNAME = "username";
    private final String KEY_PASSWORD = "password";

    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;

    public UserManager(Context context)
    {
        mPrefs = context.getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }

    public boolean checkLoginValidate(String username, String password, int Id) {
        String realUsername = mPrefs.getString(KEY_USERNAME,"");
        String realPassword = mPrefs.getString(KEY_PASSWORD,"");
        String realId = mPrefs.getString(KEY_ID,"");

        if((!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) && username.equals(realUsername)
                && password.equals(realPassword)) {
            return true;
        }
        return false;
    }
}
