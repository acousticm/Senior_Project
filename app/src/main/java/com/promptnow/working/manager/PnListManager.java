package com.promptnow.working.manager;

import android.content.Context;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;
import com.promptnow.working.model.PnItemCollectionDao;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PnListManager {

    private static PnListManager instance;

    public static PnListManager getInstance() {
        if (instance == null)
            instance = new PnListManager();
        return instance;
    }

    private Context mContext;
    private PnItemCollectionDao dao;

    private PnListManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public PnItemCollectionDao getDao() {
        return dao;
    }

    public void setDao(PnItemCollectionDao dao) {
        this.dao = dao;
    }
}
