package com.promptnow.working.manager.http;

import com.promptnow.working.model.EmployeeResponse;
import com.promptnow.working.model.GetYearModel;
import com.promptnow.working.model.Graph;
import com.promptnow.working.model.HistoryItemCollectionDao;
import com.promptnow.working.model.LeaveType;
import com.promptnow.working.model.MSG;
import com.promptnow.working.model.PnItemCollectionDao;
import com.promptnow.working.model.PushNotification;
import com.promptnow.working.model.SendLeave;
import com.promptnow.working.model.UpdateModel;
import com.promptnow.working.model.ValidateDate;
import com.promptnow.working.model.getApprovalHistoryItemCollectionDao;
import com.promptnow.working.model.getApprovalItemCollectionDao;
import com.promptnow.working.model.getApprovalName;
import com.promptnow.working.model.registerToken;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Zephy on 3/6/2017.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("Login.php")
    Call<MSG> userLogIn(@Field("email") String email, @Field("password") String password);

    @POST("EmployeeTeam.php")
    Call<PnItemCollectionDao> loadPnList();

    @FormUrlEncoded
    @POST("EmployeeProfile.php")
    Call<EmployeeResponse> getEmployee(@Field("employeeID") String empId);

    @FormUrlEncoded
    @POST("DashboardProfile.php")
    Call<EmployeeResponse> getDashboardEmp(@Field("employeeID") String empId);

    @FormUrlEncoded
    @POST("checkLeader.php")
    Call<EmployeeResponse> checkLeader(@Field("employeeID") String empId);

    @FormUrlEncoded
    @POST("getYear.php")
    Call<GetYearModel> GetYearSpinner(@Field("employeeID") String empId);


    @FormUrlEncoded
    @POST("getTotalLeave.php")
    Call<Graph> getTotalLeave(@Field("employeeID") String empId);

    @GET("LeaveType.php")
    Call<LeaveType> leavetypelist();

    @FormUrlEncoded
    @POST("employeeUpdate.php")
    Call<UpdateModel> updateEmployee(@Field("id") String id,
                                     @Field("email") String email,
                                     @Field("phoneNo1") String phoneNo1,
                                     @Field("phoneNo2") String phoneNo2,
                                     @Field("lineID") String lineID,
                                     @Field("imgProfileName") String imgProfileName,
                                     @Field("imgProfileURL") String imgProfileURL);

    @FormUrlEncoded
    @POST("leavecancel.php")
    Call<UpdateModel> ApprovalLeave(@Field("empLeaveID") String empLeaveID,
                                    @Field("leaveStatus") int leaveStatus,
                                    @Field("comment") String comment,
                                    @Field("approvalBy") String approvalBy);

    @FormUrlEncoded
    @POST("getApprovalName.php")
    Call<getApprovalName> getApprovalName(@Field("id") String Id);


    @FormUrlEncoded
    @POST("deleteLeave.php")
    Call<UpdateModel> deleteLeave(@Field("empLeaveID") String empLeaveID);


    @FormUrlEncoded
    @POST("SendLeaveImg.php")
    Call<SendLeave> SendLeaveImg(@Field("leaveDateFrom") String LeaveDateFrom,
                                 @Field("leaveDateTo") String LeaveDateTo,
                                 @Field("leaveNum") int leaveNum,
                                 @Field("leaveComment") String leaveComment,
                                 @Field("employeeID") String employeeID,
                                 @Field("headPerson") String headPerson,
                                 @Field("createdOn") String createdOn,
                                 @Field("leaveID") int leaveID,
                                 @Field("image_name") String image_name,
                                 @Field("encoded_string") String leaveImg);

    @FormUrlEncoded
    @POST("registerToken.php")
    Call<registerToken> addToken(@Field("employeeId") String employeeId,
                                 @Field("token") String token);

    @FormUrlEncoded
    @POST("ValidateDate.php")
    Call<ValidateDate> ValidateDate(@Field("id") String id);


    @FormUrlEncoded
    @POST("Push_Notification.php")
    Call<PushNotification> CallPush(@Field("employeeId") String employeeId, @Field("message") String message);

    @FormUrlEncoded
    @POST("Push_NotificationApproval.php")
    Call<PushNotification> CallPush2(@Field("message") String message,
                                     @Field("empLeaveID") String empLeaveID);

    // Send employeeId check headPerson
    @FormUrlEncoded
    @POST("leaveHistory.php")
    Call<HistoryItemCollectionDao> getLeaveHistory(@Field("id") String id);

    @FormUrlEncoded
    @POST("getLeaveApproval.php")
    Call<getApprovalItemCollectionDao> getLeaveApproval(@Field("id") String id, @Field("empId") String empId);

    @FormUrlEncoded
    @POST("getApprovalHistory.php")
    Call<getApprovalHistoryItemCollectionDao> getLeaveApprovalHistory(@Field("id") String id, @Field("empId") String empId);
}