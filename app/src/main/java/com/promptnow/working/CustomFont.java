package com.promptnow.working;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Zephy on 8/24/2017.
 */

public class CustomFont extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        /*CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
        .setDefaultFontPath("fonts/Prompt-Regular.ttf" + "Prompt-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());*/
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Prompt-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
