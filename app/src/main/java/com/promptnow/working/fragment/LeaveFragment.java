package com.promptnow.working.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.databinding.FragmentLeaveBinding;
import com.promptnow.working.view.SlidingTabLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static java.security.AccessController.getContext;
import static java.security.AccessController.getContext;
import static java.security.AccessController.getContext;
import static java.security.AccessController.getContext;
import static java.security.AccessController.getContext;
import static java.security.AccessController.getContext;

/**
 * Created by Zephy on 8/23/2017.
 */

public class LeaveFragment extends Fragment implements TabLayout.OnTabSelectedListener {

    FragmentLeaveBinding binding;
    private Session session;
    ViewPager viewPager;
    TabLayout slidingTabLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new Session(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leave, container, false);
        View root = binding.getRoot();
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
        viewPager = (ViewPager) root.findViewById(R.id.viewPager);
        slidingTabLayout = (TabLayout) root.findViewById(R.id.slidingTabLayout);
        if (getActivity() != null) {
            String a = getActivity().getString(R.string.createleave);
            String b = getActivity().getString(R.string.approvalleave);
            String c = getActivity().getString(R.string.approvalhistory);
            if (session.getLeaderId().equals("0")) {
                slidingTabLayout.addTab(slidingTabLayout.newTab().setText(a));
            } else {
                slidingTabLayout.addTab(slidingTabLayout.newTab().setText(a));
                slidingTabLayout.addTab(slidingTabLayout.newTab().setText(b));
                slidingTabLayout.addTab(slidingTabLayout.newTab().setText(c));
            }
        }
        slidingTabLayout.addOnTabSelectedListener(this);
        changeTabsFont(slidingTabLayout);
        slidingTabLayout.setupWithViewPager(viewPager);
        viewPager.setClipToPadding(false);
        viewPager.setPadding(0, padding, 0, 0);
        viewPager.setPageMargin(padding);
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager(), getContext()));
        return root;
    }

    private void changeTabsFont(TabLayout tabLayout) {
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/Prompt-Light.ttf");

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int k = 0; k < tabChildsCount; k++) {
                View tabViewChild = vgTab.getChildAt(k);
                if (tabViewChild instanceof TextView) {
                    ((TextView)
                            tabViewChild).setTypeface(type,
                            Typeface.NORMAL);
                }
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public static class MyAdapter extends FragmentPagerAdapter {

        private Context context;
        Session session;

        public MyAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
            session = new Session(context);
        }

        @Override
        public Fragment getItem(int position) {
            if (session.getLeaderId().equals("0")) {
                switch (position) {
                    case 0:
                        return LeaveFragmentA.newInstance();
                    default:
                        return null;
                }
            } else {
                switch (position) {
                    case 0:
                        return LeaveFragmentA.newInstance();
                    case 1:
                        return LeaveFragmentB.newInstance();
                    case 2:
                        return LeaveFragmentC.newInstance();
                    default:
                        return null;
                }
            }
        }

        @Override
        public int getCount() {
            if (session.getLeaderId().equals("0"))
                return 1;
            else
                return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String a = context.getString(R.string.createleave);
            String b = context.getString(R.string.approvalleave);
            String c = context.getString(R.string.approvalhistory);
            if (session.getLeaderId().equals("0")) {
                switch (position) {
                    case 0:
                        return a;
                    default:
                        return "";
                }
            } else {
                switch (position) {
                    case 0:
                        return a;
                    case 1:
                        return b;
                    case 2:
                        return c;
                    default:
                        return "";
                }
            }

        }

    }

}
