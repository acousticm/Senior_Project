package com.promptnow.working.fragment;

import android.annotation.TargetApi;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.adapter.ApprovalAdapter;
import com.promptnow.working.databinding.FragmentLeaveBBinding;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.HistoryItemDao;
import com.promptnow.working.model.getApprovalItemCollectionDao;
import com.promptnow.working.model.getApprovalItemDao;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

@TargetApi(Build.VERSION_CODES.M)
@SuppressWarnings("unused")
public class LeaveFragmentB extends Fragment {

    FragmentLeaveBBinding binding;

    // Creating a List of Result From Model
    public static ArrayList<HistoryItemDao> results = new ArrayList<HistoryItemDao>();
    private Session session;
    // Start Approval Leave //
    public static ArrayList<getApprovalItemDao> results2 = new ArrayList<>();
    private RecyclerView recyclerView2;
    private RecyclerView.Adapter adapter2;
    private RecyclerView.LayoutManager layoutManager2;
    private TextView txtData;
    LinearLayout layoutApproval;
    Button btncancel;
    // End Approval Leave//
    int pos;
    int datasize;

    public LeaveFragmentB() {
        super();
    }

    @SuppressWarnings("unused")
    public static LeaveFragmentB newInstance() {
        LeaveFragmentB fragment = new LeaveFragmentB();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        session = new Session(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leave_b, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);

        return rootView;
    }

    private void initData2() {
        results2.clear();
        binding.layoutContentContainer.showLoading();
        Call<getApprovalItemCollectionDao> call = HttpManager.getInstance().getService().getLeaveApproval(session.getLeaderId(), session.getEmployeeID());
        call.enqueue(new Callback<getApprovalItemCollectionDao>() {
            @Override
            public void onResponse(Call<getApprovalItemCollectionDao> call, Response<getApprovalItemCollectionDao> response) {
                getApprovalItemCollectionDao dao = response.body();
                if (response.isSuccessful()) {
                    for (int i = 0; i < dao.getData().size(); i++) {
                        String employeeId = dao.getData().get(i).getEmployeeId();
                        String empLeaveID = dao.getData().get(i).getEmpLeaveID();
                        String name;
                        String surname;
                        String nameType;
                        if (getActivity() != null) {
                            if (getActivity().getString(R.string.language).equals("EN")) {
                                name = dao.getData().get(i).getName();
                                surname = dao.getData().get(i).getSurname();
                                nameType = dao.getData().get(i).getNameType();
                            } else {
                                name = dao.getData().get(i).getNameTH();
                                surname = dao.getData().get(i).getSurnameTH();
                                nameType = dao.getData().get(i).getNameTypeTH();
                            }
                            String leaveDateFrom = dao.getData().get(i).getLeaveDateFrom();
                            String leaveDateTo = dao.getData().get(i).getLeaveDateTo();
                            String leaveNum = dao.getData().get(i).getLeaveNum();
                            String leaveComment = dao.getData().get(i).getLeaveComment();
                            String leaveImg = dao.getData().get(i).getLeaveImg();
                            int leaveId = dao.getData().get(i).getLeaveID();
                            int leaveMax = dao.getData().get(i).getLeaveMax();
                            String createDate = dao.getData().get(i).getCreateDate();
                            String time = dao.getData().get(i).getTime();

                            getApprovalItemDao getApproval = new getApprovalItemDao();
                            getApproval.setLeaveID(leaveId);
                            getApproval.setEmployeeId(employeeId);
                            getApproval.setEmpLeaveID(empLeaveID);
                            getApproval.setName(name);
                            getApproval.setSurname(surname);
                            getApproval.setLeaveDateFrom(leaveDateFrom);
                            getApproval.setLeaveDateTo(leaveDateTo);
                            getApproval.setLeaveNum(leaveNum);
                            getApproval.setLeaveComment(leaveComment);
                            getApproval.setLeaveImg(leaveImg);
                            getApproval.setNameType(nameType);
                            getApproval.setLeaveMax(leaveMax);
                            getApproval.setCreateDate(createDate);
                            getApproval.setTime(time);
                            results2.add(getApproval);
                        }
                    }
                }else {
                    binding.layoutContentContainer.showError();
                }

                binding.layoutContentContainer.showContent();
                recyclerView2.setAdapter(adapter2);
                if (dao.getData().size() <= 0)
                    txtData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<getApprovalItemCollectionDao> call, Throwable t) {
                binding.layoutContentContainer.showEmpty();
            }
        });

    }


    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        // Approval //
        txtData = (TextView) rootView.findViewById(R.id.txtNoData);
        recyclerView2 = (RecyclerView) rootView.findViewById(R.id.recyclerView2);
        layoutApproval = (LinearLayout) rootView.findViewById(R.id.layoutApproval);
        recyclerView2.setHasFixedSize(true);
        layoutManager2 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(layoutManager2);
        adapter2 = new ApprovalAdapter(results2, getContext());
        //recyclerView2.setAdapter(adapter2);
    }

    @Override
    public void onResume() {
        super.onResume();
        initData2();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }
}
