package com.promptnow.working.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.promptnow.working.HolidayCalculator;
import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.Singleton;
import com.promptnow.working.activity.LoginActivity;
import com.promptnow.working.adapter.DashboardAdapter;
import com.promptnow.working.adapter.HistoryAdapter;
import com.promptnow.working.adapter.SpinnerYearAdapter;
import com.promptnow.working.databinding.FragmentDashboardBinding;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.EmployeeResponse;
import com.promptnow.working.model.GetYearModel;
import com.promptnow.working.model.Graph;
import com.promptnow.working.model.HistoryItemCollectionDao;
import com.promptnow.working.model.HistoryItemDao;
import com.promptnow.working.model.registerToken;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class DashboardFragment extends Fragment implements HistoryAdapter.CallbackData {

    FragmentDashboardBinding binding;

    private Session session;
    private static ArrayList<Graph.Graph_> graphlist = new ArrayList<>();

    // Id From Server
    String empId;
    // Device Token
    String token = FirebaseInstanceId.getInstance().getToken();
    String startDate;
    Boolean leaveStatus;
    DashboardAdapter adapter; // loading circle chart
    //Creating View
    Resources res;
    public static ArrayList<HistoryItemDao> results = new ArrayList<>();
    private static ArrayList<Graph.Graph_> lstGraph = new ArrayList<>();
    private static ArrayList<GetYearModel.Year> lstYear = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    boolean leave1;
    boolean leave2;
    boolean leave3;
    int yearselect;
    HistoryAdapter historyAdapter;
    String dummy = "";
    CircularImageView imgProfile;
    SpinnerYearAdapter spinnerAdapter;

    // End LeaveHistory //
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);
        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        session = new Session(getContext());
        res = getContext().getResources();
        if (!session.loggedin()) {
            Logout();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        FirebaseMessaging.getInstance().subscribeToTopic("test");
        empId = session.getEmployeeID();
        adapter = new DashboardAdapter(getContext(), graphlist);


        return rootView;
    }

    private void LoadGraph() {
        graphlist.clear();
        // Calculate Holiday Leave By StartWork Date
        HolidayCalculator.getInstance().CalculateHoliday(startDate);
        getTotalLeave(session.getEmployeeID());
    }

    //Check Token From Device
    private void checkToken() {
        Call<registerToken> call = HttpManager.getInstance().getService().addToken(empId, token);
        call.enqueue(new Callback<registerToken>() {
            @Override
            public void onResponse(Call<registerToken> call, Response<registerToken> response) {
                if (response.isSuccessful()) {
                    session.setToken(token);
                }
            }

            @Override
            public void onFailure(Call<registerToken> call, Throwable t) {
                Log.d(TAG, "Token Fail: " + t.toString());
            }
        });
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        imgProfile = (CircularImageView) rootView.findViewById(R.id.empPic);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
            }
        });
    }

    private void CallAPI() {
        getEmployee();
        getLeaveHistory();
        historyAdapter = new HistoryAdapter(results, getContext(), this);
        getYear();
        checkToken();
        checkLeader();
        SpinnerYearOnClick();
    }

    private void SpinnerYearOnClick() {
        binding.spinneryear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.layoutContentContainer.showLoading();
                if (getActivity() != null) {
                    if (getActivity().getResources().getString(R.string.language).equals("EN")) {
                        if (parent.getItemIdAtPosition(position) > 0) {
                            yearselect = Integer.parseInt(lstYear.get(position).getYear());
                            historyAdapter.filterData(dummy, String.valueOf(yearselect));
                        } else {
                            binding.txtheadleavetype.setText("Leave History");
                            historyAdapter.filterData("","");
                            historyAdapter.loadData();
                        }
                    } else {
                        if (parent.getItemIdAtPosition(position) > 0) {
                            yearselect = (Integer.parseInt(lstYear.get(position).getYear()) - 543);
                            historyAdapter.filterData(dummy, String.valueOf(yearselect));
                        } else {
                            binding.txtheadleavetype.setText("ประวัติการลา");
                            historyAdapter.filterData("","");
                            historyAdapter.loadData();
                        }
                    }
                }
                binding.layoutContentContainer.showContent();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void Logout() {
        String a = getString(R.string.textconfirm);
        String b = getString(R.string.textcancel);
        String c = getString(R.string.titlelogout);
        String d = getString(R.string.contentlogout);
        final SweetAlertDialog ConfirmDialog = new SweetAlertDialog(getContext(),
                SweetAlertDialog.WARNING_TYPE);
        ConfirmDialog.setTitleText(c);
        ConfirmDialog.setContentText(d);
        ConfirmDialog.setConfirmText(a);
        ConfirmDialog.setCancelText(b);
        ConfirmDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                ConfirmDialog.dismissWithAnimation();
                MethodLogout();
            }
        }).show();
        ConfirmDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                ConfirmDialog.cancel();
            }
        });


    }

    private void MethodLogout() {
        Intent i = new Intent(getActivity(), LoginActivity.class);
        session.setLoggedin(false);
        startActivity(i);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void checkLeader() {
        Call<EmployeeResponse> call = HttpManager.getInstance().getService().checkLeader(empId);
        call.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                if (getActivity() != null) {
                    if (response.body().getData().size() != 0) {
                        session.setLeaderId(response.body().getData().get(0).getApproverID());
                    } else if (response.body().getData().size() == 0) {
                        session.setLeaderId("0");
                    }
                }
            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {

            }
        });
    }

    private void getEmployee() {
        binding.layoutContentContainer.showLoading();
        Call<EmployeeResponse> call = HttpManager.getInstance().getService().getDashboardEmp(empId);
        call.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                if (response.isSuccessful()) {

                    String imgProfileURL = response.body().getData().get(0).getImgProfile();
                    if (getActivity() != null) {
                        Glide.with(getContext())
                                .load(imgProfileURL)
                                .asBitmap()
                                .placeholder(R.drawable.profileimg)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.empPic);
                    }
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            binding.empName.setText(response.body().getData().get(0).getName());
                            binding.empSurname.setText(response.body().getData().get(0).getSurname());
                        } else {
                            binding.empName.setText(response.body().getData().get(0).getNameTH());
                            binding.empSurname.setText(response.body().getData().get(0).getSurnameTH());
                        }
                    }

                    session.setheadPerson(response.body().getData().get(0).getHeadPersonID());
                    startDate = response.body().getData().get(0).getStartDate();

                    binding.empPosition.setText(" " + response.body().getData().get(0).getPosition());
                    binding.empDepartment.setText(" " + response.body().getData().get(0).getDepartment());
                    session.setheadPerson(response.body().getData().get(0).getHeadPersonID());
                    Singleton.getInstance().setFullname(
                            response.body().getData().get(0).getName() + " " +
                                    response.body().getData().get(0).getSurname());
                    try {
                        LoadGraph();
                        binding.gridview.setOnItemClickListener((parent, view, position, id) -> {
                            if (getActivity() != null) {
                                if (getActivity().getString(R.string.language).equals("EN")) {
                                    if (graphlist.get(position).getLeaveID() == 1) { // Medical
                                        historyAdapter.filterData("Medical", String.valueOf(yearselect));
                                        dummy = "Medical";
                                        binding.txtheadleavetype.setText("Medical Leave");
                                    }
                                    if (graphlist.get(position).getLeaveID() == 2) { // Personal
                                        historyAdapter.filterData("Personal", String.valueOf(yearselect));
                                        dummy = "Personal";
                                        binding.txtheadleavetype.setText("Personal Leave");
                                    }
                                    if (graphlist.get(position).getLeaveID() == 3) { // Holiday
                                        historyAdapter.filterData("Holiday", String.valueOf(yearselect));
                                        dummy = "Holiday";
                                        binding.txtheadleavetype.setText("Holiday Leave");
                                    }
                                } else {
                                    if (graphlist.get(position).getLeaveID() == 1) { // Medical
                                        historyAdapter.filterData("ลาป่วย", String.valueOf(yearselect));
                                        dummy = "ลาป่วย";
                                        binding.txtheadleavetype.setText("ลาป่วย");
                                    }
                                    if (graphlist.get(position).getLeaveID() == 2) { // Personal
                                        historyAdapter.filterData("ลากิจ", String.valueOf(yearselect));
                                        binding.txtheadleavetype.setText("ลากิจ");
                                    }
                                    if (graphlist.get(position).getLeaveID() == 3) { // Holiday
                                        historyAdapter.filterData("ลาพักร้อน", String.valueOf(yearselect));
                                        binding.txtheadleavetype.setText("ลาพักร้อน");
                                    }
                                }
                                if (leaveStatus) {
                                    binding.txtNoData.setVisibility(View.GONE);
                                } else {
                                    binding.txtNoData.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    } catch (NullPointerException e) {
                        MethodLogout();
                    }

                }
                binding.layoutContentContainer.showContent();
            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });
    }

    public void getTotalLeave(String empId) {
        lstGraph.clear();
        if (getActivity() != null) {
            String tempload = getActivity().getString(R.string.loading);
        }
        //binding.layoutContentContainer.showLoading();
        Call<Graph> call = HttpManager.getInstance().getService().getTotalLeave(empId);
        call.enqueue(new Callback<Graph>() {
            @Override
            public void onResponse(Call<Graph> call, Response<Graph> response) {

                leave1 = false;
                leave2 = false;
                leave3 = false;
                String type[] = res.getStringArray(R.array.leavetype);
                Graph.Graph_ graph = new Graph.Graph_(graphlist);
                String nameType;

                if (response.body().getStatus()) {
                    if (getActivity() != null) {
                        for (int i = 0; i < response.body().getGraph().size(); i++) {
                            int leavetypeId = response.body().getGraph().get(i).getLeaveID();
                            int leavemax = response.body().getGraph().get(i).getLeaveMax();
                            int leavetotal = response.body().getGraph().get(i).getTotal();
                            int id = response.body().getGraph().get(i).getId();

                            if (getActivity().getString(R.string.language).equals("EN")) {
                                nameType = (String) response.body().getGraph().get(i).getNameType();
                            } else {
                                nameType = (String) response.body().getGraph().get(i).getNameTypeTH();
                            }

                            Graph.Graph_ data = new Graph.Graph_(lstGraph);
                            data.setLeaveID(leavetypeId);
                            data.setLeaveMax(leavemax);
                            data.setTotal(leavetotal);
                            data.setNameType(nameType);
                            data.setId(id);
                            lstGraph.add(data);
                        }
                        int index = 0;
                        for (Graph.Graph_ d : lstGraph) {
                            Graph.Graph_ data2 = new Graph.Graph_(lstGraph);
                            if (d.getId() == 0 && lstGraph.get(0).getLeaveMax() != 0) { // Medical
                                data2.setLeaveID(lstGraph.get(index).getLeaveID());
                                data2.setNameType(lstGraph.get(index).getNameType());
                                data2.setTotal(lstGraph.get(index).getTotal());
                                data2.setLeaveMax(lstGraph.get(index).getLeaveMax());
                            } else if (d.getId() == 0 && lstGraph.get(0).getLeaveMax() == 0) {
                                data2.setLeaveID(1);
                                data2.setNameType(type[0]);
                                data2.setTotal(0);
                                data2.setLeaveMax(30);
                            }
                            if (d.getId() == 1 && lstGraph.get(1).getLeaveMax() != 0) {// Personal Leave
                                data2.setLeaveID(lstGraph.get(index).getLeaveID());
                                data2.setNameType(lstGraph.get(index).getNameType());
                                data2.setTotal(lstGraph.get(index).getTotal());
                                data2.setLeaveMax(lstGraph.get(index).getLeaveMax());
                            } else if (d.getId() == 1 && lstGraph.get(1).getLeaveMax() == 0) {
                                data2.setLeaveID(2);
                                data2.setNameType(type[1]);
                                data2.setTotal(0);
                                data2.setLeaveMax(6);
                            }
                            if (d.getId() == 2 && lstGraph.get(2).getLeaveMax() != 0) { // Holiday Leave
                                data2.setLeaveID(lstGraph.get(index).getLeaveID());
                                data2.setNameType(lstGraph.get(index).getNameType());
                                data2.setLeaveMax(HolidayCalculator.getInstance().getMaxHolidayLeave());
                                data2.setTotal(lstGraph.get(index).getTotal());
                            } else if (d.getId() == 2 && lstGraph.get(2).getLeaveMax() == 0) {
                                data2.setLeaveID(2);
                                data2.setNameType(type[2]);
                                data2.setTotal(0);
                                data2.setLeaveMax(HolidayCalculator.getInstance().getMaxHolidayLeave());
                                //Singleton.getInstance().setHolidayTotal(graph.getTotal());
                                //Singleton.getInstance().setHolidayMax(graph.getLeaveMax());
                            }
                            index++;
                            graphlist.add(data2);
                        }

                        binding.gridview.setAdapter(adapter);
                        binding.layoutContentContainer.showContent();
                    } else { // Not Found Data
                        //
                    }
                }

            }

            @Override
            public void onFailure(Call<Graph> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });
    }

    public void getLeaveHistory() {
        binding.layoutContentContainer.showLoading();
        results.clear();
        String tempload = getString(R.string.loading);
        Call<HistoryItemCollectionDao> call = HttpManager.getInstance().getService().getLeaveHistory(session.getEmployeeID());
        call.enqueue(new Callback<HistoryItemCollectionDao>() {
            @Override
            public void onResponse(Call<HistoryItemCollectionDao> call, Response<HistoryItemCollectionDao> response) {
                HistoryItemCollectionDao dao = response.body();
                if (response.isSuccessful()) {
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String empLeaveID = dao.getData().get(i).getEmpLeaveID();
                                String employeeID = dao.getData().get(i).getEmployeeID();
                                String name = dao.getData().get(i).getName();
                                String surname = dao.getData().get(i).getSurname();
                                String dateStart = dao.getData().get(i).getLeaveDateFrom();
                                String dateEnd = dao.getData().get(i).getLeaveDateTo();
                                int leaveStatus = dao.getData().get(i).getLeaveStatus();
                                int leaveID = dao.getData().get(i).getLeaveID();
                                int leaveNum = dao.getData().get(i).getLeaveNum();
                                String headPerson = dao.getData().get(i).getHeadPerson();
                                String leaveComment = dao.getData().get(i).getLeaveComment();
                                String leaveImg = dao.getData().get(i).getLeaveImg();
                                String nameType = dao.getData().get(i).getNameType();
                                String comment = dao.getData().get(i).getComment();
                                String approvalBy = dao.getData().get(i).getApprovalBy();
                                String createDate = dao.getData().get(i).getCreateDate();
                                String updateDate = dao.getData().get(i).getUpdateDate();
                                String updateTime = dao.getData().get(i).getUpdateTime();

                                HistoryItemDao history = new HistoryItemDao();
                                history.setEmpLeaveID(empLeaveID);
                                history.setEmployeeID(employeeID);
                                history.setName(name);
                                history.setSurname(surname);
                                history.setLeaveDateFrom(dateStart);
                                history.setLeaveDateTo(dateEnd);
                                history.setLeaveStatus(leaveStatus);
                                history.setLeaveID(leaveID);
                                history.setLeaveNum(leaveNum);
                                history.setHeadPerson(headPerson);
                                history.setLeaveComment(leaveComment);
                                history.setLeaveImg(leaveImg);
                                history.setNameType(nameType);
                                history.setComment(comment);
                                history.setApprovalBy(approvalBy);
                                history.setCreateDate(createDate);
                                history.setUpdateDate(updateDate);
                                history.setUpdateTime(updateTime);
                                results.add(history);
                            }
                        } else {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String empLeaveID = dao.getData().get(i).getEmpLeaveID();
                                String employeeID = dao.getData().get(i).getEmployeeID();
                                String name = dao.getData().get(i).getNameTH();
                                String surname = dao.getData().get(i).getSurnameTH();
                                String dateStart = dao.getData().get(i).getLeaveDateFrom();
                                String dateEnd = dao.getData().get(i).getLeaveDateTo();
                                int leaveStatus = dao.getData().get(i).getLeaveStatus();
                                int leaveID = dao.getData().get(i).getLeaveID();
                                int leaveNum = dao.getData().get(i).getLeaveNum();
                                String headPerson = dao.getData().get(i).getHeadPerson();
                                String leaveComment = dao.getData().get(i).getLeaveComment();
                                String leaveImg = dao.getData().get(i).getLeaveImg();
                                String nameType = dao.getData().get(i).getNameTypeTH();
                                String comment = dao.getData().get(i).getComment();
                                String approvalBy = dao.getData().get(i).getApprovalBy();
                                String createDate = dao.getData().get(i).getCreateDate();
                                String updateDate = dao.getData().get(i).getUpdateDate();
                                String updateTime = dao.getData().get(i).getUpdateTime();

                                HistoryItemDao history = new HistoryItemDao();
                                history.setEmpLeaveID(empLeaveID);
                                history.setEmployeeID(employeeID);
                                history.setName(name);
                                history.setSurname(surname);
                                history.setLeaveDateFrom(dateStart);
                                history.setLeaveDateTo(dateEnd);
                                history.setLeaveStatus(leaveStatus);
                                history.setLeaveID(leaveID);
                                history.setLeaveNum(leaveNum);
                                history.setHeadPerson(headPerson);
                                history.setLeaveComment(leaveComment);
                                history.setLeaveImg(leaveImg);
                                history.setNameType(nameType);
                                history.setComment(comment);
                                history.setApprovalBy(approvalBy);
                                history.setCreateDate(createDate);
                                history.setUpdateDate(updateDate);
                                history.setUpdateTime(updateTime);
                                results.add(history);
                            }
                        }

                        recyclerView.setAdapter(historyAdapter);
                        if (dao.getData().size() <= 0)
                            binding.txtNoData.setVisibility(View.VISIBLE);
                    }

                }
                binding.layoutContentContainer.showContent();

            }

            @Override
            public void onFailure(Call<HistoryItemCollectionDao> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });

    }

    private void getYear() {
        lstYear.clear();
        Call<GetYearModel> call = HttpManager.getInstance().getService().GetYearSpinner(session.getEmployeeID());
        call.enqueue(new Callback<GetYearModel>() {
            @Override
            public void onResponse(Call<GetYearModel> call, Response<GetYearModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        if (getActivity() != null) {
                            GetYearModel.Year yeardata2 = new GetYearModel.Year();
                            if (getActivity().getResources().getString(R.string.language).equals("EN")) {
                                yeardata2.setYear("All");
                                lstYear.add(yeardata2);
                                for (int i = 0; i < response.body().getYear().size(); i++) {
                                    String yearNO = response.body().getYear().get(i).getYear();
                                    GetYearModel.Year yeardata = new GetYearModel.Year();
                                    yeardata.setYear(yearNO);
                                    lstYear.add(yeardata);
                                }
                            } else {
                                yeardata2.setYear("ทั้งหมด");
                                lstYear.add(yeardata2);
                                for (int i = 0; i < response.body().getYear().size(); i++) {
                                    int convertYear = Integer.parseInt(response.body().getYear().get(i).getYear()) + 543;
                                    String yearNO = String.valueOf(convertYear);
                                    GetYearModel.Year yeardata = new GetYearModel.Year();
                                    yeardata.setYear(yearNO);
                                    lstYear.add(yeardata);
                                }
                            }
                            spinnerAdapter = new SpinnerYearAdapter(lstYear, getContext());
                            binding.spinneryear.setAdapter(spinnerAdapter);
                            spinnerAdapter.notifyDataSetChanged();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<GetYearModel> call, Throwable t) {
                Toast.makeText(getContext(), "Error" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        CallAPI();
    }

    /*
         * Save Instance State Here
         */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void SendData(boolean status) {
        this.leaveStatus = status;
    }
}
