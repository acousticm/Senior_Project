package com.promptnow.working.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.activity.DashboardActivity;
import com.promptnow.working.databinding.FragmentEditprofileBinding;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.EmployeeResponse;
import com.promptnow.working.model.UpdateModel;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileFragment extends Fragment implements View.OnClickListener {

    FragmentEditprofileBinding binding;

    // Declare Variable //
    private Session session;
    private static final int PICK_IMAGE = 100;
    private static final String TAG = "Test";

    String empId;
    String email, phoneNo1, phoneNo2, lineID;
    String email2, phoneNo12, phoneNo22, lineID2;
    String ImageProfile;
    String imgProfileName;
    CircularImageView imgProfile;
    private String lastChar;

    @SuppressWarnings("unused")
    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        session = new Session(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_editprofile, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        empId = session.getEmployeeID();
        getEmployee(empId);
        binding.btnexitupdate.setOnClickListener(this);
        binding.btnEdit.setOnClickListener(this);
        binding.btnupdate.setOnClickListener(this);
        binding.btneditimage.setOnClickListener(this);

        return rootView;
    }

    private void hideUpdate() {
        binding.btneditimage.setVisibility(View.GONE);
        binding.boxbottomUpdate.setVisibility(View.GONE);
        binding.tveditemail.setEnabled(false);
        binding.tveditemail.setBackground(getResources().getDrawable(R.color.backgroundstd));
        binding.tveditphone1.setEnabled(false);
        binding.tveditphone1.setBackground(getResources().getDrawable(R.color.backgroundstd));
        binding.tveditphone2.setEnabled(false);
        binding.tveditphone2.setBackground(getResources().getDrawable(R.color.backgroundstd));
        binding.tveditlineId.setEnabled(false);
        binding.tveditlineId.setBackground(getResources().getDrawable(R.color.backgroundstd));
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        imgProfile = (CircularImageView) rootView.findViewById(R.id.empPic);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    ////////////////////////     Function      /////////////////////////////

    private void openGallery() {
        Intent gallery = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                Glide.with(this)
                        .load(uri)
                        .asBitmap()
                        .placeholder(R.drawable.profileimg)
                        .centerCrop()
                        .atMost()
                        .override(512, 512)
                        .into(binding.empPic);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                imgProfileName = timeStamp + ".JPEG";
            }

        }
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    private void getEmployee(String empId) {

        String tempload = getContext().getString(R.string.loading);
        final SweetAlertDialog LoadingDialog = new SweetAlertDialog(getContext(),
                SweetAlertDialog.PROGRESS_TYPE);
        LoadingDialog.setTitleText(tempload);
        LoadingDialog.setCancelable(false);
        //LoadingDialog.show();
        binding.layoutContentContainer.showLoading();
        Call<EmployeeResponse> call = HttpManager.getInstance().getService().getEmployee(empId);
        call.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                if (response.isSuccessful()) {
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            binding.tveditname.setText(response.body().getData().get(0).getName());
                            binding.tveditsurname.setText(response.body().getData().get(0).getSurname());
                            binding.tveditnickname.setText(response.body().getData().get(0).getNickname());
                        } else {
                            binding.tveditname.setText(response.body().getData().get(0).getNameTH());
                            binding.tveditsurname.setText(response.body().getData().get(0).getSurnameTH());
                            binding.tveditnickname.setText(response.body().getData().get(0).getNicknameTH());
                        }
                        binding.tvusername.setText(response.body().getData().get(0).getUsername());
                        binding.tveditemployeeID.setText(response.body().getData().get(0).getId());
                        binding.tveditemail.setText(response.body().getData().get(0).getEmail());
                        binding.tveditphone1.setText(response.body().getData().get(0).getPhoneNo1());
                        binding.tveditphone2.setText(response.body().getData().get(0).getPhoneNo2());
                        binding.tveditlineId.setText(response.body().getData().get(0).getLineID());
                        binding.tvdepart.setText(response.body().getData().get(0).getDepartment());
                        binding.tvrole.setText(response.body().getData().get(0).getPosition());
                        ImageProfile = response.body().getData().get(0).getImgProfile();
                        email = response.body().getData().get(0).getEmail();
                        phoneNo1 = response.body().getData().get(0).getPhoneNo1();
                        phoneNo2 = response.body().getData().get(0).getPhoneNo2();
                        lineID = response.body().getData().get(0).getLineID();

                        Glide.with(getContext())
                                .load(ImageProfile)
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.profileimg)
                                .into(binding.empPic);
                    }

                    if (LoadingDialog.isShowing()) {
                        LoadingDialog.dismiss();
                    }
                    binding.layoutContentContainer.showContent();

                } else {
                    binding.layoutContentContainer.showError();
                    if (LoadingDialog.isShowing())
                        LoadingDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {
                binding.layoutContentContainer.showError();
                if (LoadingDialog.isShowing())
                    LoadingDialog.dismiss();
            }
        });
    }

    private void updateData(String empId, String email, String phoneNo1, String phoneNo2, String lineID, String imgProfileName, String imgProfileURL) {
        final String a = getString(R.string.editsuccess);
        Call<UpdateModel> call = HttpManager.getInstance().getService().updateEmployee(empId, email, phoneNo1, phoneNo2, lineID, imgProfileName, imgProfileURL);
        call.enqueue(new Callback<UpdateModel>() {
            @Override
            public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {

                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(getContext(), a, Toast.LENGTH_SHORT)
                                .show();
                        Intent i = new Intent(getActivity(), DashboardActivity.class);
                        startActivity(i);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {

                    }

            }

            @Override
            public void onFailure(Call<UpdateModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnexitupdate:
                hideUpdate();
                break;
            case R.id.btnupdate:
                UpdateProfile();
                break;
            case R.id.btnEdit:
                onEditProfile();
                break;
            case R.id.btneditimage:
                openGallery();
                break;
        }
    }

    // When Click Update Button
    private void UpdateProfile() {
        binding.empPic.buildDrawingCache();
        Bitmap bmap = binding.empPic.getDrawingCache();
        ImageProfile = encodeToBase64(bmap, Bitmap.CompressFormat.JPEG, 100);
        email2 = binding.tveditemail.getText().toString();
        phoneNo12 = binding.tveditphone1.getText().toString();
        phoneNo22 = binding.tveditphone2.getText().toString();
        lineID2 = binding.tveditlineId.getText().toString();
        updateData(empId, email2, phoneNo12, phoneNo22, lineID2, imgProfileName, ImageProfile);
    }

    private void onEditProfile() {
        binding.btneditimage.setVisibility(View.VISIBLE);
        binding.boxbottomUpdate.setVisibility(View.VISIBLE);
        binding.tveditemail.setEnabled(true);
        binding.tveditemail.setBackground(getResources().getDrawable(R.drawable.shape_text_edit));
        binding.tveditphone1.setEnabled(true);
        binding.tveditphone1.setBackground(getResources().getDrawable(R.drawable.shape_text_edit));
        binding.tveditphone1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        binding.tveditphone2.setEnabled(true);
        binding.tveditphone2.setBackground(getResources().getDrawable(R.drawable.shape_text_edit));
        binding.tveditphone2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        binding.tveditlineId.setEnabled(true);
        binding.tveditlineId.setBackground(getResources().getDrawable(R.drawable.shape_text_edit));
    }
}
