package com.promptnow.working.fragment;

import android.app.SearchManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SearchView;

import com.promptnow.working.R;
import com.promptnow.working.adapter.ExpandableListAdapter2;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.PnItemCollectionDao;
import com.promptnow.working.model.PnItemDao;
import com.santalu.emptyview.EmptyView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.SEARCH_SERVICE;

@SuppressWarnings("unused")
public class PnTeamFragment2 extends Fragment implements
        android.support.v7.widget.SearchView.OnQueryTextListener, android.support.v7.widget.SearchView.OnCloseListener {

    android.support.v7.widget.SearchView search;
    private ExpandableListView listView;
    ExpandableListAdapter2 listAdapter2;
    EmptyView layoutContentContainer;
    public static ArrayList<PnItemDao> listHeader = new ArrayList<PnItemDao>();

    public PnTeamFragment2() {
        super();
    }

    @SuppressWarnings("unused")
    public static PnTeamFragment2 newInstance() {
        PnTeamFragment2 fragment = new PnTeamFragment2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        listAdapter2 = new ExpandableListAdapter2(getContext(), listHeader);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pnteam, container, false);
        initInstances(rootView, savedInstanceState);

        listView = (ExpandableListView) rootView.findViewById(R.id.PnlistView);
        layoutContentContainer = (EmptyView) rootView.findViewById(R.id.layoutContentContainer);
        search = (android.support.v7.widget.SearchView) rootView.findViewById(R.id.searchView1);
        EditText searchView = (EditText) search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchView.setTextColor(getResources().getColor(R.color.colorWhite));
        searchView.setTextSize(12);
        searchView.setHintTextColor(getResources().getColor(R.color.colorGray));
        searchView.setHint(getString(R.string.queryhint));

        // Handle Icon Search
        ImageView searchViewIcon = (ImageView) search.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        //Get parent of gathered icon
        ViewGroup linearLayoutSearchView = (ViewGroup) searchViewIcon.getParent();
        //Remove it from the left...
        linearLayoutSearchView.removeView(searchViewIcon);
        //then put it back (to the right by default)
        linearLayoutSearchView.addView(searchViewIcon);
        // End Handle Icon

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            searchViewIcon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
        }

        View searchplate = (View) search.findViewById(android.support.v7.appcompat.R.id.search_plate);
        searchplate.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setOnCloseListener(this);
        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    listView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });
        return rootView;
    }


    private void initData() {
        String tempload = getString(R.string.loading);
        final SweetAlertDialog LoadingDialog = new SweetAlertDialog(getContext(),
                SweetAlertDialog.PROGRESS_TYPE);
        LoadingDialog.setTitleText(tempload);
        LoadingDialog.setCancelable(false);
        layoutContentContainer.showLoading();
        //LoadingDialog.show();
        //listHeader = new ArrayList<>();
        listHeader.clear();
        Call<PnItemCollectionDao> call = HttpManager.getInstance().getService().loadPnList();
        call.enqueue(new Callback<PnItemCollectionDao>() {
            @Override
            public void onResponse(Call<PnItemCollectionDao> call, Response<PnItemCollectionDao> response) {
                PnItemCollectionDao dao = response.body();
                if (response.isSuccessful()) {
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String name = dao.getData().get(i).getName();
                                String surname = dao.getData().get(i).getSurname();
                                String nickname = dao.getData().get(i).getNickname();
                                String jobposition = dao.getData().get(i).getPosition();
                                String employeeCode = dao.getData().get(i).getEmployeeID();
                                String email = dao.getData().get(i).getEmail();
                                String ImgURL = dao.getData().get(i).getImgUrl();
                                String lineId = dao.getData().get(i).getLineId();
                                String phoneNo1 = dao.getData().get(i).getPhoneno1();
                                String phoneNo2 = dao.getData().get(i).getPhoneno2();
                                String depart = dao.getData().get(i).getDepartment();

                                PnItemDao pn = new PnItemDao(listHeader);
                                pn.setName(name);
                                pn.setSurname(surname);
                                pn.setNickname(nickname);
                                pn.setPosition(jobposition);
                                pn.setDepartment(depart);
                                pn.setEmployeeID(employeeCode);
                                pn.setEmail(email);
                                pn.setLineId(lineId);
                                pn.setPhoneno1(phoneNo1);
                                pn.setPhoneno2(phoneNo2);
                                pn.setImgUrl(ImgURL);
                                listHeader.add(pn);
                                Collections.sort(listHeader, new Comparator<PnItemDao>() {
                                    @Override
                                    public int compare(PnItemDao o1, PnItemDao o2) {
                                        return o1.getName().compareTo(o2.getName());
                                    }
                                });
                            }
                        } else {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String name = dao.getData().get(i).getNameTH();
                                String surname = dao.getData().get(i).getSurnameTH();
                                String nickname = dao.getData().get(i).getNicknameTH();
                                String jobposition = dao.getData().get(i).getPosition();
                                String employeeCode = dao.getData().get(i).getEmployeeID();
                                String email = dao.getData().get(i).getEmail();
                                String ImgURL = dao.getData().get(i).getImgUrl();
                                String lineId = dao.getData().get(i).getLineId();
                                String phoneNo1 = dao.getData().get(i).getPhoneno1();
                                String phoneNo2 = dao.getData().get(i).getPhoneno2();
                                String depart = dao.getData().get(i).getDepartment();

                                PnItemDao pn = new PnItemDao(listHeader);
                                pn.setName(name);
                                pn.setSurname(surname);
                                pn.setNickname(nickname);
                                pn.setPosition(jobposition);
                                pn.setDepartment(depart);
                                pn.setEmployeeID(employeeCode);
                                pn.setEmail(email);
                                pn.setLineId(lineId);
                                pn.setPhoneno1(phoneNo1);
                                pn.setPhoneno2(phoneNo2);
                                pn.setImgUrl(ImgURL);
                                listHeader.add(pn);
                                Collections.sort(listHeader, new Comparator<PnItemDao>() {
                                    @Override
                                    public int compare(PnItemDao o1, PnItemDao o2) {
                                        return o1.getName().compareTo(o2.getName());
                                    }
                                });
                            }
                        }

                    }
                    layoutContentContainer.showContent();
                } else {
                    layoutContentContainer.showError(getString(R.string.error));
                }

                listView.setAdapter(listAdapter2);
                if (LoadingDialog.isShowing()) {
                    LoadingDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PnItemCollectionDao> call, Throwable t) {
                layoutContentContainer.showError();
            }
        });
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }


    @Override
    public boolean onClose() {
        listAdapter2.filterData("");
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        String text = query;
        listAdapter2.filterData(text);
        return false;
    }
}
