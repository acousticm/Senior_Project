package com.promptnow.working.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.adapter.ApprovalHistoryAdapter;
import com.promptnow.working.databinding.FragmentLeaveCBinding;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.getApprovalHistoryItemCollectionDao;
import com.promptnow.working.model.getApprovalHistoryItemDao;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("unused")
public class LeaveFragmentC extends Fragment {

    FragmentLeaveCBinding binding;

    private static final String TAG = "";
    private Session session;
    // Start Approval Leave //
    public static ArrayList<getApprovalHistoryItemDao> results2 = new ArrayList<>();
    private RecyclerView recyclerView2;
    private RecyclerView.Adapter adapter2;
    private RecyclerView.LayoutManager layoutManager2;
    private TextView txtData;
    LinearLayout layoutApproval;
    Button btncancel;
    // End Approval Leave//
    int pos;
    int datasize;

    public LeaveFragmentC() {
        super();
    }

    @SuppressWarnings("unused")
    public static LeaveFragmentC newInstance() {
        LeaveFragmentC fragment = new LeaveFragmentC();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        session = new Session(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leave_c, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);

        return rootView;
    }

    private void initData2() {
        results2.clear();
        binding.layoutContentContainer.showLoading();
        Call<getApprovalHistoryItemCollectionDao> call = HttpManager.getInstance().getService().getLeaveApprovalHistory(session.getLeaderId(), session.getEmployeeID());
        call.enqueue(new Callback<getApprovalHistoryItemCollectionDao>() {
            @Override
            public void onResponse(Call<getApprovalHistoryItemCollectionDao> call, Response<getApprovalHistoryItemCollectionDao> response) {
                getApprovalHistoryItemCollectionDao dao = response.body();
                if (response.isSuccessful()) {
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String employeeId = dao.getData().get(i).getEmployeeId();
                                String empLeaveID = dao.getData().get(i).getEmpLeaveID();
                                String name = dao.getData().get(i).getName();
                                String surname = dao.getData().get(i).getSurname();
                                String leaveDateFrom = dao.getData().get(i).getLeaveDateFrom();
                                String leaveDateTo = dao.getData().get(i).getLeaveDateTo();
                                int leaveNum = dao.getData().get(i).getLeaveNum();
                                String leaveComment = dao.getData().get(i).getLeaveComment();
                                String leaveImg = dao.getData().get(i).getLeaveImg();
                                String nameType = dao.getData().get(i).getNameType();
                                String comment = dao.getData().get(i).getComment();
                                String approveBy = dao.getData().get(i).getApproveBy();
                                String updateOn = dao.getData().get(i).getUpdateDate();
                                String updateTime = dao.getData().get(i).getUpdateTime();
                                int leaveAmount = dao.getData().get(i).getLeaveNum();
                                int LeaveStatus = dao.getData().get(i).getLeaveStatus();
                                int leaveId = dao.getData().get(i).getLeaveID();
                                int leaveMax = dao.getData().get(i).getLeaveMax();
                                String createDate = dao.getData().get(i).getCreateDate();
                                String time = dao.getData().get(i).getTime();

                                getApprovalHistoryItemDao getApproval = new getApprovalHistoryItemDao();
                                getApproval.setLeaveID(leaveId);
                                getApproval.setEmployeeId(employeeId);
                                getApproval.setEmpLeaveID(empLeaveID);
                                getApproval.setName(name);
                                getApproval.setSurname(surname);
                                getApproval.setComment(comment);
                                getApproval.setApproveBy(approveBy);
                                getApproval.setLeaveDateFrom(leaveDateFrom);
                                getApproval.setLeaveDateTo(leaveDateTo);
                                getApproval.setLeaveNum(leaveNum);
                                getApproval.setLeaveStatus(LeaveStatus);
                                getApproval.setLeaveComment(leaveComment);
                                getApproval.setLeaveImg(leaveImg);
                                getApproval.setNameType(nameType);
                                getApproval.setLeaveMax(leaveMax);
                                getApproval.setCreateDate(createDate);
                                getApproval.setTime(time);
                                getApproval.setUpdateDate(updateOn);
                                getApproval.setUpdateTime(updateTime);
                                results2.add(getApproval);
                            }
                        } else {
                            for (int i = 0; i < dao.getData().size(); i++) {
                                String employeeId = dao.getData().get(i).getEmployeeId();
                                String empLeaveID = dao.getData().get(i).getEmpLeaveID();
                                String name = dao.getData().get(i).getNameTH();
                                String surname = dao.getData().get(i).getSurnameTH();
                                String leaveDateFrom = dao.getData().get(i).getLeaveDateFrom();
                                String leaveDateTo = dao.getData().get(i).getLeaveDateTo();
                                int leaveNum = dao.getData().get(i).getLeaveNum();
                                String leaveComment = dao.getData().get(i).getLeaveComment();
                                String leaveImg = dao.getData().get(i).getLeaveImg();
                                String nameType = dao.getData().get(i).getNameTypeTH();
                                String comment = dao.getData().get(i).getComment();
                                String approveBy = dao.getData().get(i).getApproveBy();
                                String updateOn = dao.getData().get(i).getUpdateDate();
                                String updateTime = dao.getData().get(i).getUpdateTime();
                                int leaveAmount = dao.getData().get(i).getLeaveNum();
                                int LeaveStatus = dao.getData().get(i).getLeaveStatus();
                                int leaveId = dao.getData().get(i).getLeaveID();
                                int leaveMax = dao.getData().get(i).getLeaveMax();
                                String createDate = dao.getData().get(i).getCreateDate();
                                String time = dao.getData().get(i).getTime();

                                getApprovalHistoryItemDao getApproval = new getApprovalHistoryItemDao();
                                getApproval.setLeaveID(leaveId);
                                getApproval.setEmployeeId(employeeId);
                                getApproval.setEmpLeaveID(empLeaveID);
                                getApproval.setName(name);
                                getApproval.setSurname(surname);
                                getApproval.setComment(comment);
                                getApproval.setApproveBy(approveBy);
                                getApproval.setLeaveDateFrom(leaveDateFrom);
                                getApproval.setLeaveDateTo(leaveDateTo);
                                getApproval.setLeaveNum(leaveNum);
                                getApproval.setLeaveStatus(LeaveStatus);
                                getApproval.setLeaveComment(leaveComment);
                                getApproval.setLeaveImg(leaveImg);
                                getApproval.setNameType(nameType);
                                getApproval.setLeaveMax(leaveMax);
                                getApproval.setCreateDate(createDate);
                                getApproval.setTime(time);
                                getApproval.setUpdateDate(updateOn);
                                getApproval.setUpdateTime(updateTime);
                                results2.add(getApproval);
                            }
                        }
                        binding.layoutContentContainer.showContent();
                    }
                }else {
                    binding.layoutContentContainer.showError();
                }
                recyclerView2.setAdapter(adapter2);
                if (dao.getData().size() <= 0)
                    txtData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<getApprovalHistoryItemCollectionDao> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });

    }


    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        // Approval //
        txtData = (TextView) rootView.findViewById(R.id.txtNoData);
        recyclerView2 = (RecyclerView) rootView.findViewById(R.id.recyclerView2);
        layoutApproval = (LinearLayout) rootView.findViewById(R.id.layoutApproval);
        recyclerView2.setHasFixedSize(true);
        layoutManager2 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(layoutManager2);
        adapter2 = new ApprovalHistoryAdapter(results2, getContext());
        //recyclerView2.setAdapter(adapter2);
    }

    @Override
    public void onResume() {
        super.onResume();
        initData2();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }
}
