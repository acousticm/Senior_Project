package com.promptnow.working.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;
import com.promptnow.working.BuildConfig;
import com.promptnow.working.HolidayCalculator;
import com.promptnow.working.R;
import com.promptnow.working.Session;
import com.promptnow.working.Singleton;
import com.promptnow.working.activity.DashboardActivity;
import com.promptnow.working.adapter.SpinnerAdapter;
import com.promptnow.working.databinding.FragmentLeaveABinding;
import com.promptnow.working.manager.HttpManager;
import com.promptnow.working.model.Graph;
import com.promptnow.working.model.LeaveType;
import com.promptnow.working.model.PushNotification;
import com.promptnow.working.model.SendLeave;
import com.promptnow.working.model.ValidateDate;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class LeaveFragmentA extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "";
    FragmentLeaveABinding binding;
    public static ArrayList<ValidateDate.ValiDateModel> result = new ArrayList<ValidateDate.ValiDateModel>();
    private Session session;
    private final static int DIALOG_LEAVE = 1;
    private final static int DIALOG_BUSINESS = 2;
    private final static int DIALOG_HOLIDAY = 3;
    private final static int DIALOG_BUSINESS_END = 4;
    private final static int DIALOG_HOLIDAY_END = 5;
    private final static int DIALOG_LEAVE_END = 6;
    private String blockCharacterSet = "~#^|$%&*!<>/";
    //Permission
    final private int REQUEST_CODE_ASK_PERMISSIONS_CAMERA = 100;
    final private int REQUEST_CODE_ASK_PERMISSIONS_EXTERNAL_STORAGE = 200;
    final private int REQUEST_CODE_ASK_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 300;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_IMAGE_GALLERY = 2;
    public static List<LocalDate> dateList = new ArrayList<LocalDate>();
    int i = 0;
    int matchingIndex = -1;
    boolean Status;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateTime = dateFormat.format(new Date()); // Find todays date

    //Alert Dialog
    SweetAlertDialog pDialog;

    // Date
    LocalDate daystart;
    LocalDate dayend;
    DateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd");
    Calendar c;
    private static ArrayList<Graph.Graph_> datalist = new ArrayList<>();
    int leaveId, leaveNum, day = 0;
    String leavemax;
    public String name = "null", encoded = "null";

    // Spinner
    private static ArrayList<LeaveType.Leavetype> lstSource = new ArrayList<>();
    private SpinnerAdapter spinnerAdapter;
    String mCurrentPhotoPath = "";
    int dd, mm, yy;


    public static LeaveFragmentA newInstance() {
        LeaveFragmentA fragment = new LeaveFragmentA();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
        session = new Session(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leave_a, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
        generateData();
        ValidateDate();
        binding.leavecomment.setFilters(new InputFilter[]{filter});
        binding.leavecomment.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // if enter is pressed start calculating
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {

                    // get EditText text
                    String text = ((EditText) v).getText().toString();

                    // find how many rows it cointains
                    int editTextRowCount = text.split("\\n").length;

                    // user has input more than limited - lets do something
                    // about that
                    if (editTextRowCount >= 4) {

                        // find the last break
                        int lastBreakIndex = text.lastIndexOf("\n");

                        // compose new text
                        String newText = text.substring(0, lastBreakIndex);

                        // add new text - delete old one and append new one
                        // (append because I want the cursor to be at the end)
                        ((EditText) v).setText("");
                        ((EditText) v).append(newText);

                    }
                }
                return false;
            }
        });
        binding.btnsendleave.setOnClickListener(this);
        spinnerAdapter = new SpinnerAdapter(lstSource, getContext());
        SpinnerAdapter adapter = spinnerAdapter;
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                leaveId = lstSource.get(position).getLeaveID();
                leavemax = lstSource.get(position).getLeaveMax();

                if (leaveId == 1) {
                    ResetLeave();
                    ShowLeaveContent();
                }
                if (leaveId == 2) {
                    HideLeaveContent();
                    ResetLeave();
                }
                if (leaveId == 3) {
                    HideLeaveContent();
                    ResetLeave();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    private void ResetLeave() {
        String tempdatestart = getString(R.string.selectstart);
        String tempdateend = getString(R.string.selectend);
        dateList.clear();
        leaveNum = 0;
        binding.btndatepicker2.setVisibility(View.VISIBLE);
        binding.btnsendleave.setEnabled(false);
        binding.btndatepicker1.setText(tempdatestart);
        binding.btndatepicker2.setText(tempdateend);
        binding.btndatepicker2.setEnabled(false);
        binding.numleave.setText("0");
    }

    // Send Notification to headPerson and Send Leave
    private void TakeLeave() {
        // case img gone
        if (binding.layoutimg.getVisibility() == View.VISIBLE) {
            binding.imgLeave.buildDrawingCache();
            Bitmap bmap = binding.imgLeave.getDrawingCache();
            encoded = encodeToBase64(bmap, Bitmap.CompressFormat.JPEG, 50);
        } else {
            name = "null";
            encoded = "null";
        }
        String comment = binding.leavecomment.getText().toString();
        String tempload = getContext().getString(R.string.loading);
        final String success = getContext().getString(R.string.success);

        final SweetAlertDialog LoadingDialog = new SweetAlertDialog(getContext(),
                SweetAlertDialog.PROGRESS_TYPE);
        LoadingDialog.setTitleText(tempload);
        LoadingDialog.setCancelable(false);
        LoadingDialog.show();
        try {
            Call<SendLeave> call = HttpManager.getInstance().getService().SendLeaveImg(daystart.toString(),
                    dayend.toString(), leaveNum, comment, session.getEmployeeID(),
                    session.getHeadPerson(), currentDateTime, leaveId, name, encoded);
            call.enqueue(new Callback<SendLeave>() {
                @Override
                public void onResponse(Call<SendLeave> call, Response<SendLeave> response) {

                    if (response.body().getSuccess() == 1) {
                        // Send Leave Succeuss
                        Toast.makeText(getContext(),
                                success,
                                Toast.LENGTH_SHORT)
                                .show();
                        // send notification to leader
                        if (LoadingDialog.isShowing()) {
                            LoadingDialog.dismissWithAnimation();
                            if (session.getLeaderId().equals("0")) {
                                sendNotification();
                            } else {
                                if (LoadingDialog.isShowing())
                                    LoadingDialog.dismissWithAnimation();
                                Intent intent = new Intent(getContext(), DashboardActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }

                        }
                    } else {
                        Toast.makeText(getContext(), "กรุณาตรวจสอบใบลาใหม่อีกครั้ง", Toast.LENGTH_SHORT)
                                .show();
                        LoadingDialog.dismissWithAnimation();
                    }

                }

                @Override
                public void onFailure(Call<SendLeave> call, Throwable t) {
                    if (LoadingDialog.isShowing())
                        LoadingDialog.dismissWithAnimation();
                }
            });
        } catch (NullPointerException e) {
            if (LoadingDialog.isShowing())
                LoadingDialog.dismissWithAnimation();
            String templeavenull = getString(R.string.leavenull);
            Toast.makeText(getContext(), templeavenull, Toast.LENGTH_SHORT)
                    .show();
        }

    }

    private void sendNotification() {
        String tempload = getContext().getString(R.string.loading);
        final SweetAlertDialog LoadingDialog = new SweetAlertDialog(getContext(),
                SweetAlertDialog.PROGRESS_TYPE);
        LoadingDialog.setTitleText(tempload);
        LoadingDialog.setCancelable(false);
        LoadingDialog.show();

        Call<PushNotification> call2 = HttpManager.getInstance().getService().CallPush(session.getHeadPerson(), "Receive Leave Message");
        call2.enqueue(new Callback<PushNotification>() {
            @Override
            public void onResponse(Call<PushNotification> call, Response<PushNotification> response) {
                if (response.isSuccessful()) {
                    if (LoadingDialog.isShowing())
                        LoadingDialog.dismissWithAnimation();
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    LoadingDialog.dismissWithAnimation();
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }

            @Override
            public void onFailure(Call<PushNotification> call, Throwable t) {
                if (LoadingDialog.isShowing())
                    LoadingDialog.dismissWithAnimation();
            }
        });
    }


    private void generateData() {
        lstSource.clear();
        binding.layoutContentContainer.showLoading();
        Call<LeaveType> call = HttpManager.getInstance().getService().leavetypelist();
        call.enqueue(new Callback<LeaveType>() {
            @Override
            public void onResponse(Call<LeaveType> call, Response<LeaveType> response) {
                if (response.isSuccessful()) {
                    if (getActivity() != null) {
                        if (getActivity().getString(R.string.language).equals("EN")) {
                            for (int i = 0; i < response.body().getLeavetype().size(); i++) {
                                int LeaveID = response.body().getLeavetype().get(i).getLeaveID();
                                String LeaveName = response.body().getLeavetype().get(i).getNameType();
                                String LeaveMax = response.body().getLeavetype().get(i).getLeaveMax();

                                LeaveType.Leavetype lt = new LeaveType.Leavetype(lstSource);
                                lt.setLeaveID(LeaveID);
                                lt.setNameType(LeaveName);
                                lt.setLeaveMax(LeaveMax);
                                lstSource.add(lt);
                            }
                        } else {
                            for (int i = 0; i < response.body().getLeavetype().size(); i++) {
                                int LeaveID = response.body().getLeavetype().get(i).getLeaveID();
                                String LeaveName = response.body().getLeavetype().get(i).getNameTypeTH();
                                String LeaveMax = response.body().getLeavetype().get(i).getLeaveMax();

                                LeaveType.Leavetype lt = new LeaveType.Leavetype(lstSource);
                                lt.setLeaveID(LeaveID);
                                lt.setNameType(LeaveName);
                                lt.setLeaveMax(LeaveMax);
                                lstSource.add(lt);
                            }
                        }
                        binding.layoutContentContainer.showContent();
                    }
                    spinnerAdapter.notifyDataSetChanged();
                } else {
                    binding.layoutContentContainer.showError();
                }
            }

            @Override
            public void onFailure(Call<LeaveType> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        // Button Select Image
        binding.deleteimg.setOnClickListener(this);
        binding.imgLeave.setOnClickListener(this);
        binding.txtnoteleave.setOnClickListener(this);
        binding.btndatepicker1.setOnClickListener(this);
        binding.btndatepicker2.setOnClickListener(this);
    }

    private void HideLeaveContent() {
        binding.txtnoteleave.setVisibility(View.GONE);
        binding.contentLeave.setVisibility(View.GONE);
        binding.imgLeave.setVisibility(View.GONE);
    }

    private void ShowLeaveContent() {
        binding.txtnoteleave.setVisibility(View.VISIBLE);
        binding.contentLeave.setVisibility(View.VISIBLE);
        binding.imgLeave.setVisibility(View.VISIBLE);
    }

    private void UpdateDatePickerStart() {
        binding.btndatepicker1.setText(formatDateTime.format(c.getTime()));
        dd = c.get(Calendar.DAY_OF_MONTH);
        mm = c.get(Calendar.MONTH);
        yy = c.get(Calendar.YEAR);
    }


    private void UpdateDatePickerEnd() {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        String templimit = getString(R.string.leaveoutlimit);
        String tempmorethan = getString(R.string.morethanthree);
        String tempwarning = getContext().getString(R.string.warning);
        binding.btndatepicker2.setText(formatDateTime.format(c.getTime()));
        LocalDate weekday = daystart;
        dateList.clear();
        if (daystart.getDayOfWeek() == DateTimeConstants.SATURDAY ||
                daystart.getDayOfWeek() == DateTimeConstants.SUNDAY) {
            weekday = weekday.plusWeeks(1).withDayOfWeek(DateTimeConstants.MONDAY);
        }
        while (!weekday.isAfter(dayend)) {
            dateList.add(weekday);
            if (weekday.getDayOfWeek() == DateTimeConstants.FRIDAY)
                weekday = weekday.plusDays(3);
            else
                weekday = weekday.plusDays(1);
        }

        int totalmedicine, totalperson, totalholiday, maxholiday;
        totalmedicine = Singleton.getInstance().getMedicineTotal();
        totalperson = Singleton.getInstance().getPersonTotal();
        totalholiday = Singleton.getInstance().getHolidayTotal();
        maxholiday = HolidayCalculator.getInstance().getMaxHolidayLeave();

        if (leaveId == 1) {
            if (leaveId == 1 && (dateList.size() + totalmedicine) <= 30) {
                leaveNum = dateList.size();
                Status = true;
            } else {
                pDialog.setTitleText(tempwarning);
                pDialog.setContentText(templimit);
                pDialog.show();
                Status = false;
                ResetLeave();
            }
        }
        if (leaveId == 2) {
            if (leaveId == 2 && (dateList.size() + totalperson) <= 6) {
                leaveNum = dateList.size();
                Status = true;
            } else {
                pDialog.setTitleText(tempwarning);
                pDialog.setContentText(templimit);
                pDialog.show();
                Status = false;
                ResetLeave();
            }
        }
        if (leaveId == 3) {
            //TODO: Handle Error DIalog Again na ja dont forget
            if (leaveId == 3 && (dateList.size() + totalholiday) <= maxholiday && dateList.size() <= 3) {
                leaveNum = dateList.size();
                Status = true;
            } else if (leaveId == 3 && dateList.size() > 3) {
                pDialog.setTitleText(tempwarning);
                pDialog.setContentText(tempmorethan);
                pDialog.show();
                Status = false;
                ResetLeave();
            } else {
                pDialog.setTitleText(tempwarning);
                pDialog.setContentText(templimit);
                pDialog.show();
                Status = false;
                ResetLeave();
            }
        }
        if (dateList.size() <= 0) {
            ResetLeave();
        } else if (dateList.size() > 0 && Status) {
            binding.numleave.setText("" + dateList.size());
            binding.btnsendleave.setEnabled(true);
        }

    }

    /*
     * Restore Instance State Here
     */
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btndatepicker1:
                DatePickStart();
                break;
            case R.id.btndatepicker2:
                DatePickEnd();
                break;
            case R.id.btnsendleave:
                TakeLeave();
                break;
            case R.id.txtnoteleave:
                DialogImg();
                break;
            case R.id.deleteimg:
                binding.imgLeave.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.addphoto));
                binding.layoutimg.setVisibility(View.GONE);
                binding.txtnoteleave.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void DialogImg() {
        final String a1 = getString(R.string.takephoto);
        final String a2 = getString(R.string.addphoto);
        final String a3 = getString(R.string.gallery);
        final String a4 = getString(R.string.cancel);
        final CharSequence[] items = {a1, a3, a4};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(a2);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(a1)) {
                    List<String> permissionsNeeded = new ArrayList<String>();
                    final List<String> permissionsList = new ArrayList<String>();

                    if (!addPermission(permissionsList, Manifest.permission.CAMERA))
                        permissionsNeeded.add("Camera");
                    if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
                    if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
                        permissionsNeeded.add("READ_EXTERNAL_STORAGE");

                    if (permissionsList.size() > 0) {
                        if (permissionsNeeded.size() > 0) {
                            String msg = "You need to grant access to" + permissionsNeeded.get(0);
                            for (int i = 1; i < permissionsNeeded.size(); i++)
                                msg = msg + ", " + permissionsNeeded.get(i);
                            showMessageOKCancel(msg,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                                    REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
                                        }
                                    });
                            return;
                        }
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
                    }

                    try {
                        takePhoto();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (items[item].equals(a3)) {
                    try {
                        selectFromGallery();
                    } catch (Exception e) {
                        Toast.makeText(getContext(),
                                e.getMessage(),
                                Toast.LENGTH_LONG).show();
                        Log.e(e.getClass().getName(), e.getMessage(), e);
                    }


                } else if (items[item].equals(a4)) {
                    binding.layoutimg.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    try {
                        takePhoto();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "CAMERA_PERMISSION Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void selectFromGallery() throws IOException {
        binding.txtnoteleave.setVisibility(View.GONE);
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        binding.layoutimg.setVisibility(View.VISIBLE);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), REQUEST_IMAGE_GALLERY);
    }

    private void takePhoto() throws IOException {
        binding.txtnoteleave.setVisibility(View.GONE);
        //checkPermission();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (i.resolveActivity(getActivity().getPackageManager()) != null) {
            Uri tempuri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile());
            i.putExtra(MediaStore.EXTRA_OUTPUT, tempuri);
            binding.layoutimg.setVisibility(View.VISIBLE);
            startActivityForResult(i, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void checkPermission() {
        // For Check Camera Permission
        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = checkSelfPermission(getContext(), android.Manifest.permission.CAMERA);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                    // Display UI and wait for user interaction
                    getErrorDialog("You need to allow Camera permission." +
                                    "\nIf you disable this permission, You will not able to add attachment.",
                            getContext(),
                            true)
                            .show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                            REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
                }
                return;
            }
        }

// For Check Read External Permission.
        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Display UI and wait for user interaction
                    getErrorDialog("You need to allow Read External Storage permission." +
                            "\nIf you disable this permission, You will not able to add attachment.", getContext(), false).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS_EXTERNAL_STORAGE);
                }
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Display UI and wait for user interaction
                    getErrorDialog("You need to allow Read External Storage permission." +
                            "\nIf you disable this permission, You will not able to add attachment.", getContext(), false).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                }
                return;
            }
        }
    }

    public AlertDialog.Builder getErrorDialog(String message, Context context, final boolean isFromCamera) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(getString(R.string.app_name)).setMessage(message);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                if (Build.VERSION.SDK_INT >= 23) {
                    if (isFromCamera) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                                REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
                    } else {
                        requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CODE_ASK_PERMISSIONS_EXTERNAL_STORAGE);
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE_ASK_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                    }
                }

            }
        });
        return alertDialog;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            // Show the thumbnail on ImageView
            Uri imageUri = Uri.parse(mCurrentPhotoPath);
            File file = new File(imageUri.getPath());
            InputStream ims = null;
            try {
                ims = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            rotateImage(setReducedImageSize());
            //rotateImage(BitmapFactory.decodeStream(ims));
            // ScanFile so it will be appeared on Gallery
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{imageUri.getPath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                        }
                    });
        } else if (requestCode == REQUEST_IMAGE_GALLERY) {
            if (null == data) return;
            if (data.getData() != null) {
                Uri selectedImage = data.getData();
                Glide.with(this)
                        .load(selectedImage)
                        .override(2048, 2048)
                        .into(binding.imgLeave);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = timeStamp;
                name = imageFileName + ".JPEG";
            }
        }
    }

    private Bitmap setReducedImageSize() {
        int targetImageViewWidth = binding.imgLeave.getWidth();
        int targetImageViewHeight = binding.imgLeave.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;
        int scaleFactor = Math.min(cameraImageWidth / targetImageViewWidth, cameraImageHeight / targetImageViewHeight);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

/*        Bitmap photoReducedSizeBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        imgLeave.setImageBitmap(photoReducedSizeBitmap);*/
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp;
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        //TODO: Check Create Folder here
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".JPEG",         /* suffix */
                storageDir      /* directory */
        );
        name = imageFileName + ".JPEG";
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    private void rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = 0;
        orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        try {
            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            binding.imgLeave.setImageBitmap(rotatedBitmap);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    private void showDatePickerDialog(int id) {
        switch (id) {
            case DIALOG_LEAVE:
                DatePickerDialog d = new DatePickerDialog(getContext(), mDateSetListener,
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                c.set(Calendar.DATE, day);
                d.getDatePicker().setMinDate(c.getTimeInMillis());
                d.show();
                break;
            case DIALOG_LEAVE_END:
                DatePickerDialog d6 = new DatePickerDialog(getContext(),
                        mDateEndSetListener,
                        yy, mm, dd);
                c.set(yy, mm, dd);
                d6.getDatePicker().setMinDate(c.getTimeInMillis());
                d6.show();
                break;
            case DIALOG_BUSINESS:
                DatePickerDialog d2 = new DatePickerDialog(getContext(),
                        mDateSetListener,
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                c.set(Calendar.DATE, day + 3);
                d2.getDatePicker().setMinDate(c.getTimeInMillis());
                d2.show();
                break;
            case DIALOG_BUSINESS_END:
                DatePickerDialog d4 = new DatePickerDialog(getContext(),
                        mDateEndSetListener,
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                c.set(yy, mm, dd);
                d4.getDatePicker().setMinDate(c.getTimeInMillis());
                d4.show();
                break;
            case DIALOG_HOLIDAY:
                DatePickerDialog d3 = new DatePickerDialog(getContext(),
                        mDateSetListener,
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                c.set(Calendar.DATE, day + 3);
                d3.getDatePicker().setMinDate(c.getTimeInMillis());
                d3.show();
                break;
            case DIALOG_HOLIDAY_END:
                DatePickerDialog d5 = new DatePickerDialog(getContext(),
                        mDateEndSetListener,
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                c.set(yy, mm, dd);
                d5.getDatePicker().setMinDate(c.getTimeInMillis());
                d5.show();
                break;
        }
    }

    private void ValidateDate() {
        Call<ValidateDate> call = HttpManager.getInstance().getService().ValidateDate(session.getEmployeeID());
        call.enqueue(new Callback<ValidateDate>() {
            @Override
            public void onResponse(Call<ValidateDate> call, Response<ValidateDate> response) {
                result.clear();
                if (getActivity() != null) {
                    if (response.body().getResult().size() > 0) {
                        ValidateDate item = response.body();
                        for (int i = 0; i < response.body().getResult().size(); i++) {
                            String empLeaveID = item.getResult().get(i).getEmpLeaveID();
                            String employeeID = item.getResult().get(i).getEmployeeID();
                            String leaveDateFrom = item.getResult().get(i).getLeaveDateFrom();
                            String leaveDateTo = item.getResult().get(i).getLeaveDateTo();

                            ValidateDate.ValiDateModel model = new ValidateDate.ValiDateModel();
                            model.setEmpLeaveID(empLeaveID);
                            model.setEmployeeID(employeeID);
                            model.setLeaveDateFrom(leaveDateFrom);
                            model.setLeaveDateTo(leaveDateTo);
                            result.add(model);
                            Log.d(TAG, "onResponse: " + result.size());
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ValidateDate> call, Throwable t) {
                binding.layoutContentContainer.showError();
            }
        });
    }

    boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
        return (!testDate.before(startDate) && !testDate.after(endDate));
    }

    boolean isWithinRange2(Date testDate, Date startDate, Date endDate) {
        return (!testDate.before(startDate) && !testDate.after(endDate));
    }

    private void CheckValidate(String date1) {
        String tempequal = getString(R.string.equal);
        String tempbtnend = getString(R.string.btnSelectEnd);
        String warnning = getString(R.string.warning);
        Date currentdate = null;
        Date datefrom = null;
        Date dateTo = null;
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                try {
                    currentdate = formatDateTime.parse(date1);
                    datefrom = formatDateTime.parse(result.get(i).getLeaveDateFrom());
                    dateTo = formatDateTime.parse(result.get(i).getLeaveDateTo());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (isWithinRange(currentdate, datefrom, dateTo)) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(warnning)
                            .setContentText(tempequal)
                            .show();
                    ResetLeave();
                    i = matchingIndex;
                    binding.btndatepicker2.setEnabled(false);
                    break;
                } else {
                    UpdateDatePickerStart();
                    dateList.clear();
                    binding.numleave.setText("0");
                    binding.btndatepicker2.setText(tempbtnend);
                    binding.btndatepicker2.setEnabled(true);
                }
            }
        } else {
            UpdateDatePickerStart();
            dateList.clear();
            binding.numleave.setText("0");
            binding.btndatepicker2.setText(tempbtnend);
            binding.btndatepicker2.setEnabled(true);
        }


    }

    private void CheckValidateEnd(String date1) {
        String tempequal = getString(R.string.equal);
        String tempbtnend = getString(R.string.btnSelectEnd);
        Date currentdate = null;
        Date datefrom = null;
        Date dateTo = null;
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                try {
                    currentdate = formatDateTime.parse(date1);
                    datefrom = formatDateTime.parse(result.get(i).getLeaveDateFrom());
                    dateTo = formatDateTime.parse(result.get(i).getLeaveDateTo());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (isWithinRange2(currentdate, datefrom, dateTo)) {
                    Toast.makeText(getContext(), tempequal, Toast.LENGTH_SHORT)
                            .show();
                    ResetLeave();
                    i = matchingIndex;
                    break;
                }
            }
        } else {
            UpdateDatePickerEnd();
        }


    }

    // Date Start
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            daystart = new LocalDate(year, month + 1, dayOfMonth);
            CheckValidate(daystart.toString());
        }
    };
    //Date End
    private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dayend = new LocalDate(year, month + 1, dayOfMonth);
            UpdateDatePickerEnd();
            CheckValidateEnd(dayend.toString());
        }
    };

    private void DatePickEnd() {
        c = Calendar.getInstance();
        if (leaveId == 1) {
            showDatePickerDialog(DIALOG_LEAVE_END);
        }
        if (leaveId == 2) {
            showDatePickerDialog(DIALOG_BUSINESS_END);
        }
        if (leaveId == 3) {
            showDatePickerDialog(DIALOG_HOLIDAY_END);
        }
    }

    private void DatePickStart() {
        c = Calendar.getInstance();
        day = c.get(Calendar.DATE);
        if (leaveId == 1) {
            showDatePickerDialog(DIALOG_LEAVE);
        }
        if (leaveId == 2) {
            showDatePickerDialog(DIALOG_BUSINESS);
        }
        if (leaveId == 3) {
            showDatePickerDialog(DIALOG_HOLIDAY);
        }
    }

}
